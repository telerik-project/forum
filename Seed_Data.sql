USE [UltraForum]
GO

INSERT INTO Users(DisplayName, Email, Username, Password, ProfilePicture, IsDeleted)
VALUES
('James', 'james@gmail.com', 'james', 'HXB4EZiAacp2CCaGHW1joQ6MO38XHERBpkcupYwRcRs=', '~/Images/default_picture.png', 'False'),
('Mary', 'mary@gmail.com', 'mary', 'Mw6zHX7L9sySBnKQVnCnitrFxE/A0c82muyPWl2Op1Y=', '~/Images/default_picture.png', 'False'),
('John', 'john@gmail.com', 'john', 'CGdiYoJwNomeVMNPMSSGER4ZUlWYmazdD2BLjksiHgM=', '~/Images/default_picture.png', 'False'),
('Amanda', 'amanda@gmail.com', 'amanda', 'JL1rhNbPIdO+7dVBp8gHR13wIw3gEo8ChHHlLnWUEf0=', '~/Images/default_picture.png', 'False')

INSERT INTO UserPrivileges(UserId, Privilege, IsDeleted)
VALUES (1, 1, 'False'), (2, 2, 'False'), (3, 2, 'False'), (4, 3, 'False')

INSERT INTO Entries(AuthorId, Content, CreationTime, EditTime, IsDeleted)
VALUES
(1, 'content for first post', '2021-12-01 12:50:38.0000000', '2021-12-01 12:50:38.0000000', 'False'),
(2, 'content for second post', '2021-12-01 12:50:38.0000000', '2021-12-01 12:50:38.0000000', 'False'),
(3, 'content for third post', '2021-12-01 12:50:38.0000000', '2021-12-01 12:50:38.0000000', 'False'),
(1, 'first comment', '2021-12-01 12:50:38.0000000', '2021-12-01 12:50:38.0000000', 'False'),
(2, 'second comment', '2021-12-01 12:50:38.0000000', '2021-12-01 12:50:38.0000000', 'False'),
(3, 'third comment', '2021-12-01 12:50:38.0000000', '2021-12-01 12:50:38.0000000', 'False')

INSERT INTO Posts(EntryId, Title, IsDeleted)
VALUES
(1, 'First post made by James', 'False'),
(2, 'Second post made by Mary', 'False'),
(3, 'Thrid post made by John', 'False')

INSERT INTO Comments(EntryId, PostId, IsDeleted)
VALUES
(4, 2, 'False'), (5, 3, 'False'), (6, 1, 'False')

INSERT INTO Likes(AuthorId, CreationTime, EntryId, IsDeleted)
VALUES
(1, '2021-12-01 12:50:38.0000000', 2, 'False'),
(2, '2021-12-01 12:50:38.0000000', 3, 'False'),
(3, '2021-12-01 12:50:38.0000000', 1, 'False')