﻿using Data;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service.DtoModels;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.Models;

namespace Tests.ServicesTests
{
    [TestClass]
    public class LikeServiceTest : BaseTest
    {
        private DbContextOptions<DataContext> options;
        List<UserPrivilege> privileges;
        List<User> users;
        List<Like> likes;
        List<Entry> entries;

        [TestInitialize]
        public void Initialize()
        {
            this.options = GetOptions();

            var privilegeModels = new UserPriviligeTestModels();
            var userModels = new UserTestModels();
            var likeModels = new LikeTestModels();
            var entryModels = new EntryTestModels();

            using (var arrangeContext = new DataContext(this.options))
            {
                privileges = privilegeModels.GetModels();
                users = userModels.GetModels(privileges);
                likes = likeModels.GetModels(users);
                entries = entryModels.GetModels(users, likes);

                arrangeContext.Users.AddRange(users);
                arrangeContext.Likes.AddRange(likes);
                arrangeContext.UserPrivileges.AddRange(privileges);
                arrangeContext.Entries.AddRange(entries);
                arrangeContext.SaveChanges();
            }
        }
        private LikeDTO GetLikeDTO()
        {
            return new LikeDTO()
            {
                Author = new UserDTO(users[0]),
                AuthorId = users[0].Id,
                CreationTime = DateTime.Now,
                Id = 1,
                //Entry = new EntryDTO(entries[0]),
                EntryId = 1
            };
        }

        [TestMethod]
        public void Return_Correct_When_ParamsAreValid()
        {
            // Arrange        
            var expectedDto = GetLikeDTO();

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new LikeService(assertContext);
                var actualDto = sut.Get(1);

                Assert.IsNotNull(actualDto);
                Assert.AreEqual(expectedDto.Author.UserPrivilege.Privilege, actualDto.Author.UserPrivilege.Privilege);
                Assert.AreEqual(expectedDto.AuthorId, actualDto.AuthorId);
                Assert.AreEqual(expectedDto.Id, actualDto.Id);
            }
        }

        [TestMethod]
        public void Return_Correct_Id_When_ParamsAreValid()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new LikeService(assertContext);
                var actualDtos = sut.GetAll();

                foreach (LikeDTO actualDto in actualDtos)
                {
                    Assert.IsNotNull(actualDto);

                    Like l = likes.FirstOrDefault(x => x.Id == actualDto.Id);

                    Assert.AreEqual(l.EntryId, actualDto.EntryId);
                }
            }
        }
    }
}
