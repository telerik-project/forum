﻿using Data;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service.DtoModels;
using Service.Enums;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.Models;

namespace Tests.ServicesTests
{
    [TestClass]
    public class PostServiceTest : BaseTest
    {
        private DbContextOptions<DataContext> options;
        List<UserPrivilege> privileges;
        List<User> users;
        List<Like> likes;
        List<Entry> entries;
        List<Comment> comments;
        List<Post> posts;

        [TestInitialize]
        public void Initialize()
        {
            this.options = GetOptions();

            var privilegeModels = new UserPriviligeTestModels();
            var userModels = new UserTestModels();
            var likeModels = new LikeTestModels();
            var entryModels = new EntryTestModels();
            var commentModels = new CommentTestModels();
            var postModels = new PostTestModels();

            //Make an entry
            //Make a post
            //Make an entry
            //Make a comment - tie it to the post, tie it to the entry


            using (var arrangeContext = new DataContext(this.options))
            {
                privileges = privilegeModels.GetModels();
                users = userModels.GetModels(privileges);
                likes = likeModels.GetModels(users);
                entries = entryModels.GetModels(users, likes);
                //Order of operations here...
                posts = postModels.GetModels(entries, comments);
                comments = commentModels.GetModels(entries, posts);

                arrangeContext.Users.AddRange(users);
                arrangeContext.Likes.AddRange(likes);
                arrangeContext.UserPrivileges.AddRange(privileges);
                arrangeContext.Entries.AddRange(entries);
                arrangeContext.Posts.AddRange(posts);
                arrangeContext.Comments.AddRange(comments);
                arrangeContext.SaveChanges();
            }
        }
        private EntryDTO GetEntryDTO()
        {
            return new EntryDTO()
            {
                Id = 1,
                Author = new UserDTO(users[0]),
                AuthorId = users[0].Id,
                Content = "entry content",
                CreationTime = DateTime.Now,
                Likes = new List<LikeDTO>()
                {
                    new LikeDTO(likes[0]),
                    new LikeDTO(likes[1])
                },
            };
        }

        public CommentDTO GetCommentDTO()
        {
            return new CommentDTO
            {
                EntryId = 2,
                PostId = 1
            };
        }

        public PostDTO GetPostDTO()
        {
            return new PostDTO
            {
                EntryId = 1,
                Title = "1"
            };
        }

        [TestMethod]
        public void Return_Correct_When_ParamsAreValid()
        {
            // Arrange        
            var expectedDto = GetPostDTO();

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                var actualDto = sut.Get(1);

                Assert.IsNotNull(actualDto);
                Assert.AreEqual(expectedDto.Title, actualDto.Title);
                Assert.AreEqual(expectedDto.EntryId, actualDto.EntryId);
            }
        }

        [TestMethod]
        public void ApplySortingToComments_Should_Return_Date_Ascending()
        {
            // Arrange        
            var expected = "reply comment";

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                
                var actualDto = sut.Get(1);
                actualDto.ApplySortingToComments(CommentSortStrategy.Date_Ascending);
                
                Assert.AreEqual(expected, actualDto.Comments[1].Entry.Content);
            }
        }

        [TestMethod]
        public void ApplySortingToComments_Should_Return_Date_Descending()
        {
            // Arrange        
            var expected = "reply comment";

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);

                var actualDto = sut.Get(1);
                actualDto.ApplySortingToComments(CommentSortStrategy.Date_Descending);

                Assert.AreEqual(expected, actualDto.Comments[0].Entry.Content);
            }
        }

        [TestMethod]
        public void ApplySortingToComments_Should_Return_Likes_Ascending()
        {
            // Arrange        
            var expected = "reply comment";

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);

                var actualDto = sut.Get(1);
                actualDto.ApplySortingToComments(CommentSortStrategy.Likes_Ascending);

                Assert.AreEqual(expected, actualDto.Comments[0].Entry.Content);
            }
        }

        [TestMethod]
        public void ApplySortingToComments_Should_Return_Likes_Descending()
        {
            // Arrange        
            var expected = "reply comment";

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);

                var actualDto = sut.Get(1);
                actualDto.ApplySortingToComments(CommentSortStrategy.Likes_Descending);

                Assert.AreEqual(expected, actualDto.Comments[1].Entry.Content);
            }
        }

        [TestMethod]
        public void ToData_Should_Convert_Object()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                Assert.AreEqual("entry content", sut.Get(1).ToData().Entry.Content);
                Assert.AreEqual("second post", sut.Get(2).ToData().Entry.Content);
            }
        }

        [TestMethod]
        public void GetAll_Should_Filter_By_Username()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                var sort = PostSortStrategy.None;
                var filter = PostFilterStrategy.UserName;
                Assert.AreEqual("entry content", sut.GetAll(sort, filter, "username1").First().Entry.Content);
            }
        }

        [TestMethod]
        public void GetAll_Should_Filter_By_DisplayName()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                var sort = PostSortStrategy.None;
                var filter = PostFilterStrategy.DisplayName;
                Assert.AreEqual("second post", sut.GetAll(sort, filter, "displayName2").First().Entry.Content);
            }
        }

        [TestMethod]
        public void GetAll_Should_Filter_By_Email()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                var sort = PostSortStrategy.None;
                var filter = PostFilterStrategy.Email;
                Assert.AreEqual("second post", sut.GetAll(sort, filter, "email2@gmail.com").First().Entry.Content);
            }
        }

        [TestMethod]
        public void GetAll_Should_Return_All()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                var sort = PostSortStrategy.None;
                var filter = PostFilterStrategy.None;
                Assert.AreEqual("second post", sut.GetAll(sort, filter).First().Entry.Content);
                Assert.AreEqual(2, sut.GetAll(sort, filter).Count());
            }
        }

        [TestMethod]
        public void GetAll_Should_Return_All_Sorted_Likes_Ascending()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                var sort = PostSortStrategy.Likes_Ascending;
                var filter = PostFilterStrategy.None;
                Assert.AreEqual("second post", sut.GetAll(sort, filter).First().Entry.Content);
                Assert.AreEqual(2, sut.GetAll(sort, filter).Count());
            }
        }

        [TestMethod]
        public void GetAll_Should_Return_All_Sorted_Likes_Descending()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                var sort = PostSortStrategy.Likes_Descending;
                var filter = PostFilterStrategy.None;
                Assert.AreEqual("entry content", sut.GetAll(sort, filter).First().Entry.Content);
                Assert.AreEqual(2, sut.GetAll(sort, filter).Count());
            }
        }

        [TestMethod]
        public void GetAll_Should_Return_All_Sorted_Comments_Ascending()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                var sort = PostSortStrategy.Comments_Ascending;
                var filter = PostFilterStrategy.None;
                Assert.AreEqual("second post", sut.GetAll(sort, filter).First().Entry.Content);
                Assert.AreEqual(2, sut.GetAll(sort, filter).Count());
            }
        }

        [TestMethod]
        public void GetAll_Should_Return_All_Sorted_Comments_Descending()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                var sort = PostSortStrategy.Comments_Descending;
                var filter = PostFilterStrategy.None;
                Assert.AreEqual("entry content", sut.GetAll(sort, filter).First().Entry.Content);
                Assert.AreEqual(2, sut.GetAll(sort, filter).Count());
            }
        }

        [TestMethod]
        public void AddToDbContext_Should_Save_Data()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new PostService(assertContext);
                var user = assertContext.Users.First();
                Post newPost = new Post()
                {
                    Title = "new post",
                    IsDeleted = false,
                    Entry = new Entry()
                    {
                        Author = user,
                        AuthorId = user.Id,
                        CreationTime = DateTime.Now,
                        EditTime = DateTime.Now,
                        IsDeleted = false,
                    }
                };
                sut.AddToDbContext(newPost);
                Assert.AreEqual("new post", sut.Get(3).Title);

            }
        }
    }
}
