﻿using Data;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service.DtoModels;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.ServicesTests
{
    [TestClass]
    public class UserPrivilegeServiceTest : BaseTest
    {
        private DbContextOptions<DataContext> options;
        List<UserPrivilege> privileges;
        List<User> users;

        [TestInitialize]
        public void Initialize()
        {
            this.options = GetOptions();

            using (var arrangeContext = new DataContext(this.options))
            {

                privileges = new List<UserPrivilege>
                {
                    new UserPrivilege()
                    {
                        Id = 1,
                        UserId = 1,
                        Privilege = (int)Service.Enums.UserPrivilegeLevel.Admin
                    },
                    new UserPrivilege()
                    {
                        Id = 2,
                        UserId = 2,
                        Privilege = (int)Service.Enums.UserPrivilegeLevel.User
                    }
                };

                users = new List<User>
                {
                    new User()
                    {
                        Id = 1,
                        DisplayName = "1",
                        Username = "1",
                        Email = "1",
                        Password = "1"
                    },
                    new User()
                    {
                        Id = 2,
                        DisplayName = "2",
                        Username = "2",
                        Email = "2",
                        Password = "2"
                    }
                };

                arrangeContext.UserPrivileges.AddRange(privileges);
                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public void ReturnCorrectUserPrivilege_When_ParamsAreValid()
        {
            // Arrange        
            var expectedDto = new UserPrivilegeDTO
            {
                Id = 2,
                Privilege = Service.Enums.UserPrivilegeLevel.User
            };

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new UserPrivilegeService(assertContext);
                var actualDto = sut.Get(2);

                Assert.IsNotNull(actualDto);
                Assert.AreEqual(expectedDto.Privilege, actualDto.Privilege);
                Assert.AreEqual(expectedDto.Id, actualDto.Id);
            }
        }

        [TestMethod]
        public void CreateCorrectUserPrivilege_When_ParamsAreValid()
        {
            // Arrange        
            var expectedDto = new UserPrivilegeDTO
            {
                UserId = 1,
                Privilege = Service.Enums.UserPrivilegeLevel.User
            };

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new UserPrivilegeService(assertContext);
                
                UserPrivilegeDTO d = sut.Create(expectedDto);

                var actualDto = sut.Get(3);

                Assert.IsNotNull(actualDto);
                Assert.AreEqual(expectedDto.Privilege, actualDto.Privilege);
                Assert.AreEqual(d.Privilege, expectedDto.Privilege);

            }
        }

        [TestMethod]
        public void UpdateCorrectUserPrivilege_When_ParamsAreValid()
        {
            // Arrange        
            var expectedDto = new UserPrivilegeDTO
            {
                Id = 2,
                UserId = 1,
                Privilege = Service.Enums.UserPrivilegeLevel.User
            };

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new UserPrivilegeService(assertContext);

                var d = sut.Update(2, expectedDto);

                var actualDto = sut.Get(2);

                Assert.IsNotNull(actualDto);
                Assert.AreEqual(expectedDto.Privilege, actualDto.Privilege);
                Assert.AreEqual(expectedDto.Privilege, actualDto.Privilege);
                Assert.AreEqual(d.Privilege, expectedDto.Privilege);

            }
        }

        [TestMethod]
        public void DeleteCorrectUserPrivilege_When_ParamsAreValid()
        {

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new UserPrivilegeService(assertContext);

                sut.Delete(2);

                //Change this to assert an exception - when we add it
                Assert.ThrowsException<ArgumentException>(()=> { sut.Get(2); });

            }
        }

        [TestMethod]
        public void ReturnCorrectUserPrivileges_When_ParamsAreValid()
        {

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new UserPrivilegeService(assertContext);
                var actualDtos = sut.GetAll();

                foreach (UserPrivilegeDTO actualDto in actualDtos) {
                    Assert.IsNotNull(actualDto);

                    UserPrivilege p = privileges.FirstOrDefault(x => x.Id == actualDto.Id);

                    Assert.AreEqual((Service.Enums.UserPrivilegeLevel)p.Privilege, actualDto.Privilege);
                }
            }
        }
    }
}
