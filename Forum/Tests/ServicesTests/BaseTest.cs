﻿using Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.ServicesTests
{
    public abstract class BaseTest
    {
        [TestCleanup]
        public void Cleanup()
        {
            var options = GetOptions();
            var context = new DataContext(options);
            context.Database.EnsureDeleted();
        }

        public static DbContextOptions<DataContext> GetOptions()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase("InMemoryDB")
                .Options;
            return options;
        }
    }
}
