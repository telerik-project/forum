﻿using Data;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service.DtoModels;
using Service.Enums;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.Models;

namespace Tests.ServicesTests
{
    [TestClass]
    public class CommentServiceTest : BaseTest
    {
        private DbContextOptions<DataContext> options;
        List<UserPrivilege> privileges;
        List<User> users;
        List<Like> likes;
        List<Entry> entries;
        List<Comment> comments;
        List<Post> posts;

        [TestInitialize]
        public void Initialize()
        {
            this.options = GetOptions();

            var privilegeModels = new UserPriviligeTestModels();
            var userModels = new UserTestModels();
            var likeModels = new LikeTestModels();
            var entryModels = new EntryTestModels();
            var commentModels = new CommentTestModels();
            var postModels = new PostTestModels();

            //Make an entry
            //Make a post
            //Make an entry
            //Make a comment - tie it to the post, tie it to the entry


            using (var arrangeContext = new DataContext(this.options))
            {
                privileges = privilegeModels.GetModels();
                users = userModels.GetModels(privileges);
                likes = likeModels.GetModels(users);
                entries = entryModels.GetModels(users, likes);
                //Order of operations here...
                posts = postModels.GetModels(entries, comments);
                comments = commentModels.GetModels(entries, posts);

                arrangeContext.Users.AddRange(users);
                arrangeContext.Likes.AddRange(likes);
                arrangeContext.UserPrivileges.AddRange(privileges);
                arrangeContext.Entries.AddRange(entries);
                arrangeContext.Posts.AddRange(posts);
                arrangeContext.Comments.AddRange(comments);
                arrangeContext.SaveChanges();
            }
        }
        private EntryDTO GetEntryDTO()
        {
            return new EntryDTO()
            {
                Id = 1,
                Author = new UserDTO(users[0]),
                AuthorId = users[0].Id,
                Content = "entry content",
                CreationTime = DateTime.Now,
                Likes = new List<LikeDTO>()
                {
                    new LikeDTO(likes[0]),
                    new LikeDTO(likes[1])
                },
            };
        }

        public CommentDTO GetCommentDTO()
        {
            return new CommentDTO
            {
                EntryId = 2,
                PostId = 1
            };
        }

        [TestMethod]
        public void Return_Correct_When_ParamsAreValid()
        {
            // Arrange        
            var expectedDto = GetCommentDTO();

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new CommentService(assertContext);
                var actualDto = sut.Get(1);

                Assert.IsNotNull(actualDto);
                Assert.AreEqual(expectedDto.PostId, actualDto.PostId);
            }
        }

        [TestMethod]
        public void GetAll_Should_Return_All_Sorted_Date_Ascending()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new CommentService(assertContext);
                var sort = CommentSortStrategy.Date_Ascending;
                Assert.AreEqual("entry comment", sut.GetAll(sort).First().Entry.Content);
                Assert.AreEqual(2, sut.GetAll(sort).Count());
            }
        }

        [TestMethod]
        public void GetAll_Should_Return_All_Sorted_Date_Descending()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new CommentService(assertContext);
                var sort = CommentSortStrategy.Date_Descending;
                Assert.AreEqual("reply comment", sut.GetAll(sort).First().Entry.Content);
                Assert.AreEqual(2, sut.GetAll(sort).Count());
            }
        }

        [TestMethod]
        public void GetAll_Should_Return_All_Sorted_Likes_Ascending()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new CommentService(assertContext);
                var sort = CommentSortStrategy.Likes_Ascending;
                Assert.AreEqual("reply comment", sut.GetAll(sort).First().Entry.Content);
                Assert.AreEqual(2, sut.GetAll(sort).Count());
            }
        }

        [TestMethod]
        public void GetAll_Should_Return_All_Sorted_Likes_Descending()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new CommentService(assertContext);
                var sort = CommentSortStrategy.Likes_Descending;
                Assert.AreEqual("entry comment", sut.GetAll(sort).First().Entry.Content);
                Assert.AreEqual(2, sut.GetAll(sort).Count());
            }
        }

        [TestMethod]
        public void AddToDbContext_Should_Save_Data()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var sut = new CommentService(assertContext);
                var user = assertContext.Users.First();
                Comment newComment = new Comment()
                {
                    PostId = 4,
                    IsDeleted = false,
                    EntryId = 5,
                    Entry = new Entry()
                    {
                        Author = user,
                        AuthorId = user.Id,
                        Content = "new comment",
                        CreationTime = DateTime.Now,
                        EditTime = DateTime.Now,
                        IsDeleted = false
                    }
                };
                sut.AddToDbContext(newComment);
                Assert.AreEqual("new comment", sut.Get(3).Entry.Content);
                
            }
        }
    }
}
