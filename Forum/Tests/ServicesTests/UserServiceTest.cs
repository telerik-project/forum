﻿using Data;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service.DtoModels;
using Service.Enums;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.ServicesTests
{
    [TestClass]
    public class UserServiceTest : BaseTest
    {
        private DbContextOptions<DataContext> options;
        List<UserPrivilege> privileges;
        List<User> users;

        [TestInitialize]
        public void Initialize()
        {
            this.options = GetOptions();

            using (var arrangeContext = new DataContext(this.options))
            {

                privileges = new List<UserPrivilege>
                {
                    new UserPrivilege()
                    {
                        Id = 1,
                        UserId = 1,
                        Privilege = 1
                    },
                    new UserPrivilege()
                    {
                        Id = 2,
                        UserId = 2,
                        Privilege = 2
                    }
                };

                users = new List<User>
                {
                    new User()
                    {
                        Id = 1,
                        DisplayName = "displayName1",
                        Username = "username1",
                        Email = "email1@gmail.com",
                        Password = "123123123",
                        UserPrivilege = new UserPrivilege()
                        {
                            Id = 1,
                            UserId = 1,
                            Privilege = 1
                        }
                    },
                    new User()
                    {
                        Id = 2,
                        DisplayName = "displayName2",
                        Username = "username2",
                        Email = "email2@gmail.com",
                        Password = "123123123",
                        UserPrivilege = new UserPrivilege()
                        {
                            Id = 2,
                            UserId = 2,
                            Privilege = 2
                        }
                    }
                };

                arrangeContext.Users.AddRange(users);
                arrangeContext.SaveChanges();
            }
        }
        private UserDTO GetUserDTO()
        {
            return new UserDTO()
            {
                Id = 2,
                DisplayName = "displayName",
                Email = "email@gmail.com",
                Password = "123123123",
                UserPrivilege = new UserPrivilegeDTO(privileges[1])
            };
        }
        [TestMethod]
        public void ReturnCorrectUserPrivilege_When_ParamsAreValid()
        {
            // Arrange        
            var expectedDto = GetUserDTO();

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext,likeService,postService,commentService);
                var actualDto = sut.Get(2);

                Assert.IsNotNull(actualDto);
                Assert.AreEqual(expectedDto.UserPrivilege.Privilege, actualDto.UserPrivilege.Privilege);
                Assert.AreEqual(expectedDto.Id, actualDto.Id);
            }
        }

        [TestMethod]
        public void CreateCorrectUser_When_ParamsAreValid()
        {
            // Arrange        
            var expectedDto = GetUserDTO();

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);

                UserDTO d = sut.Create(expectedDto);

                var actualDto = sut.Get(3);

                Assert.IsNotNull(actualDto);
                Assert.AreEqual(expectedDto.UserPrivilege.Privilege, actualDto.UserPrivilege.Privilege);
                Assert.AreEqual(d.UserPrivilege.Privilege, expectedDto.UserPrivilege.Privilege);

            }
        }

        [TestMethod]
        public void UpdateCorrectUser_When_ParamsAreValid()
        {
            // Arrange        
            var expectedDto = GetUserDTO();

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);

                var d = sut.Update(2, expectedDto);

                var actualDto = sut.Get(2);

                Assert.IsNotNull(actualDto);
                Assert.AreEqual(expectedDto.UserPrivilege.Privilege, actualDto.UserPrivilege.Privilege);
                Assert.AreEqual(d.Password, expectedDto.Password);

            }
        }

        [TestMethod]
        public void DeleteCorrectUserPrivilege_When_ParamsAreValid()
        {

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);

                sut.Delete(2);

                //Change this to assert an exception - when we add it
                Assert.ThrowsException<ArgumentException>(() => { sut.Get(2); });

            }
        }

        [TestMethod]
        public void ReturnCorrectUserPrivileges_When_ParamsAreValid()
        {

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);
                var actualDtos = sut.GetAll();

                foreach (UserDTO actualDto in actualDtos)
                {
                    Assert.IsNotNull(actualDto);

                    UserPrivilege p = privileges.FirstOrDefault(x => x.Id == actualDto.Id);

                    Assert.AreEqual((Service.Enums.UserPrivilegeLevel)p.Privilege, actualDto.UserPrivilege.Privilege);
                }
            }
        }

        [TestMethod]
        public void Return_Correct_Privilige_When_GetByUsername()
        {

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);

                Assert.AreEqual(Service.Enums.UserPrivilegeLevel.Admin , sut.GetByUsername("username1").UserPrivilege.Privilege);

            }
        }

        [TestMethod]
        public void Create_Should_Auto_Set_Privilege()
        {

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);
                sut.Create(new UserDTO()
                {
                    DisplayName = "display3",
                    Email = "email3@gmai.com",
                    Password = "Password7!",
                    Username = "username3",
                    UserPrivilege = new UserPrivilegeDTO()
                });
                Assert.AreEqual(UserPrivilegeLevel.User, sut.Get(3).UserPrivilege.Privilege);
            }
        }

        [TestMethod]
        public void Create_Should_Throw_When_Email_Exists()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);

                Assert.ThrowsException<ArgumentException>(() => {
                    sut.Create(new UserDTO()
                    {
                        DisplayName = "display1233",
                        Email = "email1@gmail.com",
                        Password = "Password7!",
                        Username = "username",
                        UserPrivilege = new UserPrivilegeDTO()
                    });
                });
            }
        }

        [TestMethod]
        public void Create_Should_Throw_When_Username_Exists()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);
                Assert.ThrowsException<ArgumentException>(() => {
                    sut.Create(new UserDTO()
                    {
                        DisplayName = "display1231233",
                        Email = "email1121233@gmail.com",
                        Password = "Password7!",
                        Username = "username1",
                        UserPrivilege = new UserPrivilegeDTO()
                    });
                });
            }
        }

        [TestMethod]
        public void Create_Should_Throw_When_DisplayName_Exists()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);
                Assert.ThrowsException<ArgumentException>(() => {
                    sut.Create(new UserDTO()
                    {
                        DisplayName = "displayName1",
                        Email = "email111133@gmail.com",
                        Password = "Password7!",
                        Username = "username1231",
                        UserPrivilege = new UserPrivilegeDTO()
                    });
                });
            }
        }

        [TestMethod]
        public void SetProfilePicture_Should_Set_String()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);
                var user = sut.Get(1);
                sut.SetProfilePicture(user.Username, "wwwroot/Images/picture.jpg");
                Assert.AreEqual("wwwroot/Images/picture.jpg", sut.Get(1).ProfilePicture);
            }
        }

        [TestMethod]
        public void GetByUsername_Should_Throw_If_No_User_Found()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);
                var user = sut.Get(1);
                sut.SetProfilePicture(user.Username, "wwwroot/Images/picture.jpg");
                Assert.ThrowsException<ArgumentException>(() => { sut.GetByUsername("asdasddsa"); });
            }
        }

        [TestMethod]
        public void Authenticate_User_Should_Return_Correctly()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);
                sut.Create(new UserDTO 
                {
                        Id = 3,
                        DisplayName = "displayName3",
                        Username = "username3",
                        Email = "email3@gmail.com",
                        Password = "jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg="
                });
                Assert.AreEqual("displayName3", sut.AuthenticateUser("username3", "admin").DisplayName);
            }
        }

        [TestMethod]
        public void GetAll_Should_Filter_By_Username()
        {

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);
                var filterStrategy = UserFilterStrategy.UserName;
                var actualDtos = sut.GetAll(filterStrategy, "username1");
                Assert.AreEqual("username1", actualDtos.FirstOrDefault().Username);
            }
        }

        [TestMethod]
        public void GetAll_Should_Filter_By_DisplayName()
        {

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);
                var filterStrategy = UserFilterStrategy.DisplayName;
                var actualDtos = sut.GetAll(filterStrategy, "displayName1");
                Assert.AreEqual("displayName1", actualDtos.FirstOrDefault().DisplayName);
            }
        }

        [TestMethod]
        public void GetAll_Should_Filter_By_Email()
        {

            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var sut = new UserService(assertContext, likeService, postService, commentService);
                var filterStrategy = UserFilterStrategy.Email;
                var actualDtos = sut.GetAll(filterStrategy, "email2@gmail.com");
                Assert.AreEqual("email2@gmail.com", actualDtos.FirstOrDefault().Email);
            }
        }
    }
}
