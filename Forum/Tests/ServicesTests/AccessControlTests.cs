﻿using Data;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service.Enums;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.Models;

namespace Tests.ServicesTests
{
    [TestClass]
    public class AccessControlTests : BaseTest
    {
        private DbContextOptions<DataContext> options;
        List<User> users;
        List<UserPrivilege> privileges;

        [TestInitialize]
        public void Initialize()
        {
            this.options = GetOptions();
            var privilegeModels = new UserPriviligeTestModels();
            var userModels = new UserTestModels();

            using (var arrangeContext = new DataContext(this.options))
            {
                privileges = privilegeModels.GetModels();
                users = userModels.GetModels(privileges);
                arrangeContext.Users.AddRange(users);
                arrangeContext.UserPrivileges.AddRange(privileges);
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public void TryGetByUsername_Should_Return_DTO()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var userService = new UserService(assertContext, likeService, postService, commentService);
                var sut = new AccessControl(userService);
                Assert.AreEqual("username2", sut.TryGetByUsername("username2").Username);
                Assert.AreEqual("displayName2", sut.TryGetByUsername("username2").DisplayName);
                Assert.AreEqual("UserDTO", sut.TryGetByUsername("username2").GetType().Name);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Username not found.")]
        public void TryGetByUsername_Should_Throw_If_Not_Found()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var userService = new UserService(assertContext, likeService, postService, commentService);
                var sut = new AccessControl(userService);
                sut.TryGetByUsername("qwertyasdf");
            }
        }

        [TestMethod]
        public void GetUserPrivilegeLevel_Should_Return_Correct_Privilege()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var userService = new UserService(assertContext, likeService, postService, commentService);
                var sut = new AccessControl(userService);
                Assert.AreEqual(UserPrivilegeLevel.Admin, sut.GetUserPrivilegeLevel("username1"));
            }
        }

        [TestMethod]
        public void IsAdmin_Should_Return_Correct_Boolean()
        {
            // Act & Assert
            using (var assertContext = new DataContext(this.options))
            {
                var likeService = new LikeService(assertContext);
                var postService = new PostService(assertContext);
                var commentService = new CommentService(assertContext);
                var userService = new UserService(assertContext, likeService, postService, commentService);
                var sut = new AccessControl(userService);
                Assert.AreEqual(true, sut.IsAdmin(userService.Get(1)));
                Assert.AreEqual(false, sut.IsAdmin(userService.Get(2)));
            }
        }
    }
}
