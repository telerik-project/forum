﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Models
{
    public class EntryTestModels
    {
        public List<Entry> GetModels(List<User> users, List<Like> likes)
        {
            return new List<Entry>()
            {
                new Entry() //1st post
                {
                    Author = users[0],
                    AuthorId = users[0].Id,
                    CreationTime = DateTime.Now,
                    Content = "entry content",
                    Likes = new List<Like>(){ likes[0]},
                    Id = 1
                },
                new Entry() // comment
                {
                    Author = users[0],
                    AuthorId = users[0].Id,
                    CreationTime = DateTime.Now,
                    Content = "entry comment",
                    Likes = new List<Like>(){ likes[1]},
                    Id = 2
                },
                new Entry() // 2nd reply comment
                {
                    Author = users[1],
                    AuthorId = users[1].Id,
                    CreationTime = DateTime.Now.AddHours(1),
                    Content = "reply comment",
                    Id = 3
                },
                new Entry() // 2nd post
                {
                    Author = users[1],
                    AuthorId = users[1].Id,
                    CreationTime = DateTime.Now.AddHours(1),
                    Content = "second post",
                    Id = 4
                }
            };
        }
    }
}
