﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Models
{
    public class PostTestModels
    {
        public List<Post> GetModels(List<Entry> entries, List<Comment> comments)
        {
            return new List<Post>()
            {
                new Post()
                {
                    Entry = entries[0],
                    EntryId = entries[0].Id,
                    Title = "1",
                    Id = 1
                },
                new Post()
                {
                    Entry = entries[3],
                    EntryId = entries[3].Id,
                    Title = "2",
                    Id = 2
                }
            };
        }
    }
}
