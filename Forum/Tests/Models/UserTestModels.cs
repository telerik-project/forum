﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Models
{
    public class UserTestModels
    {
        public List<User> GetModels(List<UserPrivilege> privileges)
        {
            return new List<User>()
            {
                new User()
                {
                    Id = 1,
                    DisplayName = "displayName1",
                    Username = "username1",
                    Email = "email1@gmail.com",
                    Password = "123123123",
                    UserPrivilege = privileges[0]
                },
                new User()
                {
                    Id = 2,
                    DisplayName = "displayName2",
                    Username = "username2",
                    Email = "email2@gmail.com",
                    Password = "123123123",
                    UserPrivilege = privileges[1]
                }
            };
        }
    }
}
