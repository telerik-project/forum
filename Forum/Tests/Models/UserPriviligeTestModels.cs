﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Models
{
    public class UserPriviligeTestModels
    {
        public List<UserPrivilege> GetModels()
        {
            return new List<UserPrivilege>()
            {
                new UserPrivilege()
                {
                    Id = 1,
                    UserId = 1,
                    Privilege = 1,
                },
                new UserPrivilege()
                {
                    Id = 2,
                    UserId = 2,
                    Privilege = 2,
                }
            };
        }
    }
}
