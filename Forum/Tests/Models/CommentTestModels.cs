﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Models
{
    public class CommentTestModels
    {
        public List<Comment> GetModels(List<Entry> entries, List<Post> posts)
        {
            return new List<Comment>()
            {
                new Comment()
                {
                    Entry = entries[1],
                    EntryId = entries[1].Id,
                    //Post = posts[0],
                    PostId = posts[0].Id,
                    Id = 1
                },
                new Comment()
                {
                    Entry = entries[2],
                    EntryId = entries[2].Id,
                    PostId = posts[0].Id,
                    Id = 2
                }
            };
        }
    }
}
