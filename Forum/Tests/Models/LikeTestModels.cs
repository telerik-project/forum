﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Models
{
    public class LikeTestModels
    {
        public List<Like> GetModels(List<User> users)
        {
            return new List<Like>()
            {
                new Like()
                {
                    Author = users[0],
                    AuthorId = users[0].Id,
                    CreationTime = DateTime.Now,
                    Id = 1,
                    EntryId = 1
                },
                new Like()
                {
                    Author = users[1],
                    AuthorId = users[1].Id,
                    CreationTime = DateTime.Now,
                    Id = 2,
                    EntryId = 2
                }
            };
        }
    }
}
