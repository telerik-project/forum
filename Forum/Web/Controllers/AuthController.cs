﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.ViewModels;

namespace Web.Controllers
{
    public class AuthController : Controller
    {

        const string USER_ID = "UserId";
        const string ADMINISTRATOR = "Administrator";
        const string USER_NAME = "CurrentUser";

        static UserViewModel _currentUser;
        public static UserViewModel CurrentUser
        {
            get{
                return _currentUser;
            }
        }

        private readonly IUserService userService;
        public AuthController(IUserService userService)
        {
            this.userService = userService;
        }
        public IActionResult Login()
        {
            if(this.HttpContext.Session.GetString("CurrentUser") != null)
            {
                return RedirectToAction("Index", "Posts");
            }

            var loginViewModel = new LoginViewModel();

            return this.View(loginViewModel);
        }

        [HttpPost]
        public IActionResult Login([Bind("Username, Password")] LoginViewModel loginViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(loginViewModel);
            }

            try
            {
                var user = this.userService.AuthenticateUser(loginViewModel.Username, loginViewModel.Password);
                if (user == null)
                {
                    throw new NullReferenceException("Invalid credentials.");
                }

                _currentUser = new UserViewModel(user);

                switch (user.UserPrivilege.Privilege)
                {
                    case UserPrivilegeLevel.Admin:
                        this.HttpContext.Session.SetInt32("Administrator", 1);
                        this.HttpContext.Session.SetInt32("UserId", user.Id);
                        break;

                    case UserPrivilegeLevel.User:
                        this.HttpContext.Session.SetInt32("UserId", user.Id);
                        break;

                    case UserPrivilegeLevel.Blocked:
                        this.HttpContext.Session.SetInt32("UserId", user.Id);
                        break;
                }

                this.HttpContext.Session.SetString("CurrentUser", user.Username);

                return RedirectToAction("Index", "Posts");

            }
            catch (ArgumentException)
            {
                TempData["error"] = "Invalid login credentials.";
                return RedirectToAction("Login", loginViewModel);
            }
        }

        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");
            this.HttpContext.Session.Remove("Administrator");
            this.HttpContext.Session.Remove("UserId");
            _currentUser = null;

            return this.RedirectToAction("index", "home");
        }

        public IActionResult Register()
        {
            var registerViewModel = new RegisterViewModel();

            return this.View(registerViewModel);
        }

        [HttpPost]
        public IActionResult Register([Bind("Username, DisplayName, Email, Password, ConfirmPassword")] RegisterViewModel registerViewModel)
        {

            if (!this.ModelState.IsValid)
            {
                TempData["error"] = "Please fill all fields.";
                return RedirectToAction("Register", registerViewModel);
            }

            if (!EmailValidator.EmailIsValid(registerViewModel.Email))
            {
                TempData["error"] = "Invalid email.";
                return RedirectToAction("Register", registerViewModel);
            }

            if (!PasswordValidator.IsValidPassword(registerViewModel.Password))
            {
                TempData["error"] = "Password must be at least 8 symbols and should contain capital letter, digit and special symbol.";
                return RedirectToAction("Register", registerViewModel);
            }

            try
            {
                this.userService.CheckForAvailability(registerViewModel.Username, registerViewModel.Email, registerViewModel.DisplayName);
            }
            catch (ArgumentException e)
            {
                if (e.Message.Equals("Username already taken."))
                {
                    this.ModelState.AddModelError("Username", "Username already taken.");
                    return this.View(registerViewModel);
                }
                if (e.Message.Equals("Display name already taken."))
                {
                    this.ModelState.AddModelError("DisplayName", "Display name already taken.");
                    return this.View(registerViewModel);
                }
                if (e.Message.Equals("Email already taken."))
                {
                    this.ModelState.AddModelError("Email", "Email already taken.");
                    return this.View(registerViewModel);
                }
            }
            
            this.userService.Create(new UserDTO()
            {
                DisplayName = registerViewModel.DisplayName,
                Email = registerViewModel.Email,
                Password = PasswordValidator.HashPassword(registerViewModel.Password),
                Username = registerViewModel.Username,
                UserPrivilege = new UserPrivilegeDTO()
                {
                    Privilege = UserPrivilegeLevel.User
                },
                ProfilePicture = "~/Images/default_picture.png"
            });
            return this.RedirectToAction(nameof(this.Login));
        }

    }
}
