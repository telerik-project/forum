﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Web.ViewModels;

namespace Web.Controllers
{
    public class SettingsController : Controller
    {
        private readonly IUserService userService;
        private UserViewModel currentUser;
        public SettingsController(IUserService userService)
        {
            this.userService = userService;
        }
        public IActionResult Index()
        {
            if (this.HttpContext.Session.GetString("CurrentUser") == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            currentUser = new UserViewModel(this.userService.GetByUsername(this.HttpContext.Session.GetString("CurrentUser")));
            return View(currentUser);
        }

        [HttpPost]
        public IActionResult UploadImage()
        {
            currentUser = new UserViewModel(this.userService.GetByUsername(this.HttpContext.Session.GetString("CurrentUser")));

            if (Request.Form.Files.Count() > 1)
            {
                return RedirectToAction("Index", new { currentUser, error = TempData["error"] = "No more than 1 file allowed" });
            }
            if (Request.Form.Files.Count() == 0)
            {
                return RedirectToAction("Index", new { currentUser, error = TempData["error"] = "No file chosen." });
            }

            string fileExt = System.IO.Path.GetExtension(Request.Form.Files[0].FileName);
            if (fileExt == ".jpeg" || fileExt == ".jpg" || fileExt == ".png" || fileExt == ".bmp")
            {
                if (Request.Form.Files[0].Length <= 100000)
                {

                    Guid g = Guid.NewGuid();

                    var currentPath = System.IO.Directory.GetCurrentDirectory();
                    var targetPath = currentPath + "\\wwwroot\\Images\\" + g + fileExt;
                    var relativePath = "~/Images/" + g + fileExt;

                    var updatedUser = this.userService.SetProfilePicture(currentUser.UserName, relativePath);
                    using (FileStream fs = System.IO.File.Create(targetPath))
                    {
                        Request.Form.Files[0].CopyTo(fs);
                        fs.Flush();
                    }
                    return RedirectToAction("Index", "Settings", new UserViewModel(new UserDTO(updatedUser)));
                }
                else
                {
                    return RedirectToAction("Index", new { currentUser, error = TempData["error"] = "File size must be less than 100 KB." });
                }
            }
            else
            {
                return RedirectToAction("Index", new { currentUser, error = TempData["error"] = "Allowed formats: .jpg .jpeg .png .bmp" });
            }
        }
    }
}
