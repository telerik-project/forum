﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Service.Services;
using Service.Contracts;
using Web.ViewModels;
using Service.DtoModels;
using Service.Enums;
using System.IO;

namespace Web.Controllers
{
    public class UsersController : BaseController
    {
        private readonly IUserService _context;
        public const int PAGE_SIZE = 5;

        public UsersController(IUserService context) : base(context)
        {
            _context = context;
        }
        // GET: Users/Delete/5
        public IActionResult DeleteUser(int id, UserFilterStrategy filterStrategy = UserFilterStrategy.None, string filterData = null, int page = 0)
        {
            UserFilterStrategy fStrat = filterStrategy;
            string fData = filterData;
            int pg = page;

            this._context.Delete(id);
            //WE need to handle a situation in which the user deletes themselves. Boot them back to the home page - and clear the current authorization stuff

            return RedirectToAction("Index", "Users", new { filterStrategy = fStrat, filterData = fData, page = pg });
        }

        // GET: Users
        public IActionResult Index(UserFilterStrategy filterStrategy = UserFilterStrategy.None, string filterData = null, int page = 0)
        {

            if (this.GetLoggedInUser() == null)
            {
                return RedirectToAction("Login", "Auth");
            }

            int fromEntry = -1;

            if(page > -1)
            {
                fromEntry = page * PAGE_SIZE;
            }
            else
            {
                page = -1;
            }

            UserListViewModel users = new UserListViewModel(_context.GetAll(filterStrategy, filterData, fromEntry, PAGE_SIZE));

            int totalModels = _context.GetRecordCount();

            int totalPages = (int)Math.Ceiling((float)totalModels / (float)PAGE_SIZE);

            if (filterData == null)
            {
                filterStrategy = UserFilterStrategy.None;
            }

            ViewData.Add("FilterStrategy", filterStrategy);
            ViewData.Add("FilterData", filterData);
            ViewData.Add("PageSize", PAGE_SIZE);
            ViewData.Add("CurrentPage", page);
            ViewData.Add("TotalPages", totalPages);

            return View(users);
        }

        public IActionResult SetUserBlocked(int id, bool blocked, UserFilterStrategy filterStrategy = UserFilterStrategy.None, string filterData = null, int page = 0)
        {

            UserFilterStrategy fStrat = filterStrategy;
            string fData = filterData;
            int pg = page;

            if (blocked)
            {
                this.userService.BlockUser(id);
            }
            else
            {
                this.userService.UnblockUser(id);
            }

            return RedirectToAction("Index", "Users", new { filterStrategy =  fStrat, filterData = fData, page = pg});

        }

        // GET: Users/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            UserDTO dto = this.userService.Get(id.Value);

            if (dto == null)
            {
                return NotFound();
            }

            this.userService.PopulateUserDTO(dto);

            UserViewModel user = new UserViewModel(dto);

            return View(user);
        }

        [HttpPost]
        public IActionResult Edit(int? id, string displayName, string password)
        {
            if (id == null)
            {
                return NotFound();
            }

            UserDTO dto = this.userService.Get(id.Value);

            if (dto == null)
            {
                return NotFound();
            }

            dto.DisplayName = displayName;
            dto.Password = password;

            this.userService.Update(id.Value, dto);

            return RedirectToAction("Details", "Users", new { id = id.Value });
        }


        [HttpPost]
        public IActionResult UploadImage(int id)
        {

            UserViewModel currentUser = new UserViewModel(this.userService.Get(id));

            if (Request.Form.Files.Count() > 1)
            {
                return RedirectToAction("Index", new { currentUser, error = TempData["error"] = "No more than 1 file allowed" });
            }
            if (Request.Form.Files.Count() == 0)
            {
                return RedirectToAction("Index", new { currentUser, error = TempData["error"] = "No file chosen." });
            }

            string fileExt = System.IO.Path.GetExtension(Request.Form.Files[0].FileName);
            if (fileExt == ".jpeg" || fileExt == ".jpg" || fileExt == ".png" || fileExt == ".bmp")
            {
                if (Request.Form.Files[0].Length <= 100000)
                {

                    Guid g = Guid.NewGuid();

                    var currentPath = System.IO.Directory.GetCurrentDirectory();
                    var targetPath = currentPath + "\\wwwroot\\Images\\" + g + fileExt;
                    var relativePath = "~/Images/" + g + fileExt;

                    var updatedUser = this.userService.SetProfilePicture(currentUser.UserName, relativePath);
                    using (FileStream fs = System.IO.File.Create(targetPath))
                    {
                        Request.Form.Files[0].CopyTo(fs);
                        fs.Flush();
                    }
                    int uId = id;
                    return RedirectToAction("Details", "Users", new { id = uId });
                }
                else
                {
                    return RedirectToAction("Index", new { currentUser, error = TempData["error"] = "File size must be less than 100 KB." });
                }
            }
            else
            {
                return RedirectToAction("Index", new { currentUser, error = TempData["error"] = "Allowed formats: .jpg .jpeg .png .bmp" });
            }
        }
    }

    /*

    // GET: Users/Create
    public IActionResult Create()
    {
        return View();
    }

    // POST: Users/Create
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Username,DisplayName,Email,Password,Id,IsDeleted")] User user)
    {
        if (ModelState.IsValid)
        {
            _context.Add(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        return View(user);
    }

    // GET: Users/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var user = await _context.Users.FindAsync(id);
        if (user == null)
        {
            return NotFound();
        }
        return View(user);
    }

    // POST: Users/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Username,DisplayName,Email,Password,Id,IsDeleted")] User user)
    {
        if (id != user.Id)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(user);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(user.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }
        return View(user);
    }

    // GET: Users/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var user = await _context.Users
            .FirstOrDefaultAsync(m => m.Id == id);
        if (user == null)
        {
            return NotFound();
        }

        return View(user);
    }

    // POST: Users/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        var user = await _context.Users.FindAsync(id);
        _context.Users.Remove(user);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool UserExists(int id)
    {
        return _context.Users.Any(e => e.Id == id);
    }

    */
}

