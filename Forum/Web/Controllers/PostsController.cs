﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Service.Contracts;
using Web.ViewModels;
using Service.DtoModels;
using Service.Enums;

namespace Web.Controllers
{
    public class PostsController : BaseController
    {
        private readonly IPostService service;
        private readonly ILikeService likeService;
        private readonly IEntryService entryService;
        private readonly ICommentService commentService;

        public PostsController(IPostService service, ILikeService likeService, IEntryService entryService, ICommentService commentService, IUserService userService) : base(userService)
        {
            this.service = service;
            this.likeService = likeService;
            this.entryService = entryService;
            this.commentService = commentService;
        }

        // GET: Posts
        public IActionResult Index(int pg = 1, PostSortStrategy sortStrategy = PostSortStrategy.None, PostFilterStrategy filterStrategy =PostFilterStrategy.None, string filterData = null)
        {
            if (this.GetLoggedInUser() == null)
            {
                return RedirectToAction("Login", "Auth");
            }

            if (filterData == null)
            {
                filterStrategy = PostFilterStrategy.None;
            }


            var data = service.GetAll(sortStrategy,filterStrategy,filterData);

            foreach (PostDTO dto in data)
            {
                this.userService.PopulateUserDTO(dto.Entry.Author);
            }

            const int pageSize = 4;
            if (pg < 1)
            {
                pg = 1;
            }
            int recsCount = data.Count();
            var pager = new Pager(recsCount, pg, pageSize);
            int recSkip = (pg - 1) * pageSize;
            var dataForPage = data.Skip(recSkip).Take(pager.PageSize).ToList();
            PostSummaryListViewModel listView = new PostSummaryListViewModel(dataForPage);

            this.ViewBag.Pager = pager;

            ViewData.Add("SortStrategy", sortStrategy);
            ViewData.Add("FilterStrategy", filterStrategy);
            ViewData.Add("FilterData", filterData);

            return View(listView);

        }

        // GET: Posts/Details/5
        public IActionResult Details(int? id, CommentSortStrategy sortStrategy = CommentSortStrategy.None)
        {
            if (id == null)
            {
                return NotFound();
            }

            PostDTO p = service.Get(id.Value);
            p.ApplySortingToComments(sortStrategy);

            if (p == null)
            {
                return NotFound();
            }

            userService.PopulateUserDTO(p.Entry.Author);

            for(int i = 0; i < p.Comments.Count; i++)
            {
                userService.PopulateUserDTO(p.Comments[i].Entry.Author);
            }

            //Create a details view...

            ViewData.Add("SortStrategy", sortStrategy);  

            PostDetailsViewModel view = new PostDetailsViewModel(p);

            return View(view);
        }

        public IActionResult RemoveLike(int? id, int? postId, int? userId)
        {
            /*EntryDTO p = entryService.Get(id.Value);

            if (p == null)
            {
                return NotFound();
            }*/

            UserDTO user = GetLoggedInUser();

            likeService.Delete(id.Value);

            int pId = postId.Value;

            return this.RedirectToAction("details", "posts", new { id = pId });
        }

        public IActionResult LikePost(int? id, int? postId)
        {

            EntryDTO p = entryService.Get(id.Value);

            if (p == null)
            {
                return NotFound();
            }

            UserDTO user = GetLoggedInUser();

            LikeDTO like = new LikeDTO
            {
                AuthorId = user.Id,
                EntryId = p.Id
            };

            likeService.Create(like);

            int pId = postId.Value;

            return this.RedirectToAction("details", "posts", new { id = pId });

        }

        public IActionResult LikeComment(int? id, int? postId)
        {

            EntryDTO p = entryService.Get(id.Value);

            if (p == null)
            {
                return NotFound();
            }

            UserDTO user = GetLoggedInUser();

            LikeDTO like = new LikeDTO
            {
                AuthorId = user.Id,
                EntryId = p.Id
            };

            likeService.Create(like);

            return this.RedirectToAction("details", "posts", new { id = postId.Value });

        }

        [HttpPost]
        public IActionResult AddComment(int id, string content)
        {

            PostDTO p = service.Get(id);

            if (p == null)
            {
                return NotFound();
            }

            UserDTO user = GetLoggedInUser();

            CommentDTO comment = new CommentDTO
            {
                PostId = p.Id,
                Entry = new EntryDTO
                {
                    AuthorId = user.Id,
                    Content = content
                }

            };

            commentService.Create(comment);

            int postId = id;

            return this.RedirectToAction("details", "posts", new { id = postId });

        }

        [HttpPost]
        public IActionResult EditComment(int id, string content)
        {

            UserDTO user = GetLoggedInUser();

            //Do some sort of security here I guess

            CommentDTO comment = commentService.Get(id);

            if(comment == null)
            {
                return NotFound();
            }

            comment.Entry.Content = content;

            commentService.Update(comment.Id, comment);

            int postId = comment.PostId;

            return this.RedirectToAction("details", "posts", new { id = postId });

        }

        [HttpPost]
        public IActionResult EditPost(int id, string title, string content)
        {

            UserDTO user = GetLoggedInUser();

            //Do some sort of security here I guess

            PostDTO post = service.Get(id);

            if (post == null)
            {
                return NotFound();
            }

            post.Title = title;
            post.Entry.Content = content;

            service.Update(post.Id, post);

            int postId = post.Id;

            return this.RedirectToAction("details", "posts", new { id = postId });

        }

        public IActionResult DeletePost(int id)
        {

            UserDTO user = GetLoggedInUser();

            //Do some sort of security here I guess

            PostDTO post = service.Get(id);

            if (post == null)
            {
                return NotFound();
            }

            service.Delete(post.Id);

            return this.RedirectToAction("index", "posts");

        }

        public IActionResult DeleteComment(int id)
        {

            UserDTO user = GetLoggedInUser();

            //Do some sort of security here I guess

            CommentDTO comment = commentService.Get(id);

            if (comment == null)
            {
                return NotFound();
            }

            int postId = comment.PostId;

            commentService.Delete(comment.Id);

            return this.RedirectToAction("details", "posts", new { id = postId });

        }

        [HttpPost]
        public IActionResult CreatePost(string title, string content)
        {

            UserDTO user = GetLoggedInUser();

            PostDTO post = new PostDTO
            {
                Title = title,
                Entry = new EntryDTO
                {
                    AuthorId = user.Id,
                    Content = content
                }

            };

            PostDTO created = this.service.Create(post);

            int postId = created.Id;

            return this.RedirectToAction("details", "posts", new { id = postId });

        }

        /*
        // GET: Posts/Create
        public IActionResult Create()
        {
            ViewData["EntryId"] = new SelectList(_context.Entries, "Id", "Id");
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,EntryId,Id,IsDeleted")] Post post)
        {
            if (ModelState.IsValid)
            {
                _context.Add(post);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EntryId"] = new SelectList(_context.Entries, "Id", "Id", post.EntryId);
            return View(post);
        }

        // GET: Posts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Posts.FindAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            ViewData["EntryId"] = new SelectList(_context.Entries, "Id", "Id", post.EntryId);
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Title,EntryId,Id,IsDeleted")] Post post)
        {
            if (id != post.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(post);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostExists(post.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EntryId"] = new SelectList(_context.Entries, "Id", "Id", post.EntryId);
            return View(post);
        }

        // GET: Posts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Posts
                .Include(p => p.Entry)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var post = await _context.Posts.FindAsync(id);
            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PostExists(int id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }

        */
    }
}
