﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers
{
    public abstract class BaseController : Controller
    {

        const string USER_ID = "UserId";

        protected IUserService userService;

        public BaseController(IUserService service)
        {
            this.userService = service;
        }

        protected UserDTO GetLoggedInUser()
        {

            int? id =  HttpContext.Session.GetInt32(USER_ID);

            if(id == null)
            {
                return null;
            }

            return this.userService.Get(id.Value);

        }

    }
}
