﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.ViewModels;

namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        IPostService postService;

        public HomeController(IUserService service, IPostService postService) : base(service)
        {
            this.postService = postService;
        }

        public IActionResult Index()
        {
            //Construct our home view controller...

            //if(this.GetLoggedInUser() != null)
            //{
            //    return RedirectToAction("Index", "Posts");
            //}

            HomePageViewModel model = new HomePageViewModel();

            IEnumerable<PostDTO> recentPosts = postService.GetAll(Service.Enums.PostSortStrategy.Date_Descending, Service.Enums.PostFilterStrategy.None, "", 0, 10);
            IEnumerable<PostDTO> topPosts = postService.GetAll(Service.Enums.PostSortStrategy.Comments_Descending, Service.Enums.PostFilterStrategy.None, "", 0, 10);


            foreach (PostDTO dto in recentPosts)
            {
                this.userService.PopulateUserDTO(dto.Entry.Author);
            }


            foreach (PostDTO dto in topPosts)
            {
                this.userService.PopulateUserDTO(dto.Entry.Author);
            }

            //Optimize this - add a function in the service
            int postCount = postService.GetRecordCount();
            int userCount = userService.GetRecordCount();

            model.TopRecentPosts = new PostSummaryListViewModel(recentPosts);
            model.TopTenPosts = new PostSummaryListViewModel(topPosts);
            model.TotalPostCount = postCount;
            model.TotalUserCount = userCount;

            return View(model);
        }
    }
}
