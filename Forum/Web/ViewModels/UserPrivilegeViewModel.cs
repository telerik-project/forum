﻿using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class UserPrivilegeViewModel : BaseViewModel<UserPrivilegeDTO>
    {

        public int UserId { get; set; }
        public UserPrivilegeLevel PrivilegeLevel { get; set; }
        public UserPrivilegeViewModel() : base() { }
        public UserPrivilegeViewModel(UserPrivilegeDTO dto) : base(dto) { }

        public override UserPrivilegeDTO ToDTO()
        {
            return new UserPrivilegeDTO
            {
                UserId = UserId,
                Privilege = PrivilegeLevel
            };
        }

        protected override void FromDTO(UserPrivilegeDTO data)
        {
            this.PrivilegeLevel = data.Privilege;
            this.UserId = data.UserId;
        }
    }
}
