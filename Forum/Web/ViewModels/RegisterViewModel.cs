﻿using System.ComponentModel.DataAnnotations;
using Web.ViewModels;

namespace Web.ViewModels
{
    public class RegisterViewModel : LoginViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string DisplayName { get; set; }

        [Required]
        public string ConfirmPassword { get; set; }
    }
}
