﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class PostSummaryViewModel : BaseViewModel<PostDTO>
    {

        public int CommentCount { get; set; }
        public int LikeCount { get; set; }
        public string Title { get; set; }
        public int Id { get; set; }
        
        public EntryViewModel Entry { get; set; }

        public PostSummaryViewModel() : base() { }
        public PostSummaryViewModel(PostDTO dto) : base(dto) { }

        public override PostDTO ToDTO()
        {
            PostDTO dto = new PostDTO()
            {
                Title = this.Title,
                Entry = new EntryDTO()
                {
                    Content = this.Entry.Content
                }
            };

            return dto;
        }

        protected override void FromDTO(PostDTO data)
        {

            this.Entry = new EntryViewModel(data.Entry);
            
            CommentCount = data.Comments.Count;
            LikeCount = data.Entry.Likes.Count;
            Title = data.Title;
            Id = data.Id;
        }
    }
}
