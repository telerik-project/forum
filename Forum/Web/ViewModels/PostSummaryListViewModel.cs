﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class PostSummaryListViewModel
    {
        public List<PostSummaryViewModel> PostList { get; set; }

        public PostSummaryListViewModel()
        {
            PostList = new List<PostSummaryViewModel>();
        }

        public PostSummaryListViewModel(IEnumerable<PostDTO> posts)
        {

            PostList = new List<PostSummaryViewModel>();

            foreach(PostDTO post in posts)
            {
                PostSummaryViewModel p = new PostSummaryViewModel(post);
                PostList.Add(p);
            }

        }

    }
}
