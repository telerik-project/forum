﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class PostDetailsViewModel : PostSummaryViewModel
    {
        public List<CommentViewModel> Comments { get; set; }
      
        public PostDetailsViewModel() : base() {

            Comments = new List<CommentViewModel>();
        
        }
        public PostDetailsViewModel(PostDTO dto) : base(dto) { }

        public override PostDTO ToDTO()
        {
            PostDTO dto = base.ToDTO();

            //And then a little more

            return dto;
        }

        protected override void FromDTO(PostDTO data)
        {
            base.FromDTO(data);

            Comments = new List<CommentViewModel>();

            for (int i = 0; i < data.Comments.Count; i++)
            {
                CommentViewModel c = new CommentViewModel(data.Comments[i]);
                Comments.Add(c);
            }

        }
    }
}
