﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class EntryViewModel : BaseViewModel<EntryDTO>
    {

        public int Id { get; set; }
        public UserViewModel Author { get; set; }

        //TO DO: Add profile pic

        public DateTime CreationTime { get; set; }

        public DateTime EditTime { get; set; }

        public string Content { get; set; }

        public List<LikeViewModel> Likes { get; set; }

        //TO DO: We should really just take a 'snippet' of the content here (first line or two)
        //We don't need to show the entire content in a post summary

        public EntryViewModel() : base() { }
        public EntryViewModel(EntryDTO dto) : base(dto) { }

        public override EntryDTO ToDTO()
        {
            return new EntryDTO
            {
                Author = Author.ToDTO(),
                Content = this.Content
            };
        }

        protected override void FromDTO(EntryDTO data)
        {
            Author = new UserViewModel(data.Author);
            CreationTime = data.CreationTime;
            EditTime = data.EditTime;
            this.Content = data.Content;
            this.Id = data.Id;

            Likes = new List<LikeViewModel>();

            for (int i = 0; i < data.Likes.Count; i++)
            {
                LikeViewModel c = new LikeViewModel(data.Likes[i]);
                Likes.Add(c);
            }
        }
    }
}
