﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class UserListViewModel
    {

        public List<UserViewModel> Users { get; set; }

        public UserListViewModel() {

            Users = new List<UserViewModel>();
        
        }

        public UserListViewModel(IEnumerable<UserDTO> dto)
        {

            Users = new List<UserViewModel>();

            foreach(UserDTO u in dto)
            {
                Users.Add(new UserViewModel(u));
            }
            
        }
    }
}
