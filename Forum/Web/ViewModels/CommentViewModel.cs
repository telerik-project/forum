﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class CommentViewModel : BaseViewModel<CommentDTO>
    {

        public EntryViewModel Entry { get; set; }
        public int Id { get; set; }

        public int PostId { get; set; }

        public CommentViewModel() : base() { }
        public CommentViewModel(CommentDTO dto) : base(dto) { }

        public override CommentDTO ToDTO()
        {
            return new CommentDTO
            {
                Entry = this.Entry.ToDTO(),
                Id = this.Id,
                PostId = this.PostId
            };
        }

        protected override void FromDTO(CommentDTO data)
        {
            Entry = new EntryViewModel(data.Entry);
            Id = data.Id;
            PostId = data.PostId;
        }
    }
}
