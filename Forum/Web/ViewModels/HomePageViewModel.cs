﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class HomePageViewModel
    {
        public PostSummaryListViewModel TopTenPosts { get; set; }
        public PostSummaryListViewModel TopRecentPosts { get; set; }
        
        public int TotalUserCount { get; set; }

        public int TotalPostCount { get; set; }

        public HomePageViewModel() { }

    }
}
