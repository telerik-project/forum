﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class LikeViewModel : BaseViewModel<LikeDTO>
    {
        //It would be much better to have a UserViewModel we can use here... save typing
        public UserViewModel Author { get; set; }
        public int EntryId { get; set; }
        public int Id { get; set; }
        public LikeViewModel() : base() { }
        public LikeViewModel(LikeDTO dto) : base(dto) { }
        public override LikeDTO ToDTO()
        {

            return new LikeDTO
            {
                Id = this.Id,
                EntryId = this.EntryId
            };
        }

        protected override void FromDTO(LikeDTO data)
        {
            Author = new UserViewModel(data.Author);
            EntryId = data.EntryId;
            Id = data.Id;
        }
    }
}
