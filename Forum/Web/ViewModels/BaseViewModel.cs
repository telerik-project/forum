﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public abstract class BaseViewModel<TDTO>
    {

        public abstract TDTO ToDTO();

        protected abstract void FromDTO(TDTO data);

        public BaseViewModel() { }
        public BaseViewModel(TDTO dto)
        {
            FromDTO(dto);
        }
    }
}
