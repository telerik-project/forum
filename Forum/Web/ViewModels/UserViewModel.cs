﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class UserViewModel : BaseViewModel<UserDTO>
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string ProfilePicture { get; set; }

        public int LikeCount { get; set; }
        public int CommentCount { get; set; }
        public int PostCount { get; set; }
        
        public UserPrivilegeViewModel Privilege { get; set; }
        public UserViewModel() : base() { }
        public UserViewModel(UserDTO dto) : base(dto) { }
        public override UserDTO ToDTO()
        {
            return new UserDTO
            {
                Id = this.Id,
                Password = this.Password,
                Username = this.UserName,
                Email = this.Email,
                DisplayName = this.DisplayName,
                UserPrivilege = this.Privilege.ToDTO()
            };
        }

        protected override void FromDTO(UserDTO data)
        {
            if(data == null)
            {
                return;
            }
            this.Id = data.Id;
            this.Password = data.Password;
            this.UserName = data.Username;
            this.Privilege = new UserPrivilegeViewModel(data.UserPrivilege);
            this.DisplayName = data.DisplayName;
            this.Email = data.Email;
            this.ProfilePicture = data.ProfilePicture;
            this.LikeCount = data.LikeCount;
            this.PostCount = data.PostCount;
            this.CommentCount = data.CommentCount;
        }
    }
}
