﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using Service.Services;
using System;
using System.Collections.Generic;
using Web.Models;

namespace Web.API_Controllers
{


    public class UsersController : BaseApiController<UserDTO, UserWebModel>
    {
        /*
         * Default functionality is handled inside BaseApiController for Get, Get(id), Delete, Post
         * BaseApiController will return a webmodel if the user is not an admin
         * BaseApiController will return a DTO if the user is an admin
         * If you wish to change a function on a controller (Get, Get(id), Delete, Post)
         * Change it here
         * These functions provide you with: The data, the user, their access level (an Enum)
         * If you change these functions - you are responsible for returning an IActionResult appropriately (e.g: Ok, NotFound etc)
         * Calling base.HandleDelete, base.HandleGet, base.HandlePost etc will allow the BaseApiController to perform usual operations
         */

        public UsersController(IAccessControl accessControl, IUserService service) : base(accessControl, service) { }
        protected override IActionResult HandlePost(UserDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            if (!PasswordValidator.IsValidPassword(dataModel.Password))
            {
                return this.BadRequest("Password must be at least 8 symbols and should contain capital letter, digit and special symbol");
            }
            if (EmailValidator.EmailIsValid(dataModel.Email))
            {
                try
                {
                    dataModel.Password = PasswordValidator.HashPassword(dataModel.Password);
                    return base.HandlePost(dataModel, user, accessLevel);
                }
                catch (ArgumentException emailAlreadyExits)
                {
                    return this.BadRequest(emailAlreadyExits.Message);
                }
            }
            else
            {
                return this.BadRequest("Invalid Email.");
            }
        }

        [HttpGet("filterBy")]
        public IActionResult Get([FromHeaderAttribute] string username, [FromHeaderAttribute] string password, UserFilterStrategy filterStrategy, string filterData)
        {

            IUserService userService = service as IUserService;

            UserDTO user = null;
            UserPrivilegeLevel accessLevel = GetUserPrivilegeLevel(username, password, out user);

            var data = userService.GetAll(filterStrategy, filterData);

            return HandleGet(data, user, accessLevel);

        }
        
        [HttpPost("block")]
        public IActionResult BlockUser([FromHeaderAttribute] string username, [FromHeaderAttribute] string password, int id)
        {
            IUserService userService = service as IUserService;
            UserDTO user = null;
            UserPrivilegeLevel accessLevel = GetUserPrivilegeLevel(username, password, out user);

            if (accessLevel != UserPrivilegeLevel.Admin)
            {
                return this.BadRequest($"{user.DisplayName} is not Admin.");
            }

            try
            {
                userService.BlockUser(id);
                return this.Ok($"User with ID {id} is now blocked.");
            }
            catch (ArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        [HttpPost("unblock")]
        public IActionResult UnblockUser([FromHeaderAttribute] string username, [FromHeaderAttribute] string password, int id)
        {
            IUserService userService = service as IUserService;
            UserDTO user = null;
            UserPrivilegeLevel accessLevel = GetUserPrivilegeLevel(username, password, out user);

            if (accessLevel != UserPrivilegeLevel.Admin)
            {
                return this.BadRequest($"{user.DisplayName} is not Admin.");
            }

            try
            {
                userService.UnblockUser(id);
                return this.Ok($"User with ID {id} is now un blocked.");
            }
            catch (ArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
        }
        protected override IActionResult HandleGet(IEnumerable<UserDTO> data, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, user, accessLevel);
        }

        protected override IActionResult HandleGet(UserDTO data, int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, id, user, accessLevel);
        }

        protected override IActionResult HandleDelete(int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleDelete(id, user, accessLevel);
        }

        protected override IActionResult HandlePut(int id, UserDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            if (!PasswordValidator.IsValidPassword(dataModel.Password))
            {
                return this.BadRequest("Password must be at least 8 symbols and should contain capital letter, digit and special symbol");
            }
            if (EmailValidator.EmailIsValid(dataModel.Email))
            {
                try
                {
                    if (accessLevel == UserPrivilegeLevel.Admin)
                    {
                        dataModel.Password = PasswordValidator.HashPassword(dataModel.Password);
                        return base.HandlePut(id, dataModel, user, accessLevel);
                    }
                    else
                    {
                        if (user.Username == dataModel.Username)
                        {
                            if (dataModel.UserPrivilege.Privilege != UserPrivilegeLevel.User)
                            {
                                return this.BadRequest("Users cannot modify privilege");
                            }
                            dataModel.Password = PasswordValidator.HashPassword(dataModel.Password);
                            return base.HandlePut(id, dataModel, user, accessLevel);
                        }
                        else
                        {
                            return this.Unauthorized($"Username cannot be changed.");
                        }
                    }
                }
                catch (ArgumentException emailAlreadyExits)
                {
                    return this.BadRequest(emailAlreadyExits.Message);
                }
            }
            else
            {
                return this.BadRequest("Invalid Email.");
            }
        }

        protected override bool CanUserCreateNewRecord()
        {
            return false;
        }

        protected override bool CanUserUpdateExistingRecord()
        {
            return true;
        }

        protected override bool CanUserGetAllRecords()
        {
            return false;
        }

        protected override bool CanUserDeleteRecord()
        {
            return false;
        }
    }
}
