﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Web.Models;

namespace Web.API_Controllers
{
    public class CommentsController : BaseApiController<CommentDTO, CommentWebModel>
    {

        /*
         * Default functionality is handled inside BaseApiController for Get, Get(id), Delete, Post
         * BaseApiController will return a webmodel if the user is not an admin
         * BaseApiController will return a DTO if the user is an admin
         * If you wish to change a function on a controller (Get, Get(id), Delete, Post)
         * Change it here
         * These functions provide you with: The data, the user, their access level (an Enum)
         * If you change these functions - you are responsible for returning an IActionResult appropriately (e.g: Ok, NotFound etc)
         * Calling base.HandleDelete, base.HandleGet, base.HandlePost etc will allow the BaseApiController to perform usual operations
         */
        private readonly IPostService postService;
        public CommentsController(IAccessControl accessControl, ICommentService service, IPostService postService)
            : base(accessControl, service)
        {
            this.postService = postService;
        }

        protected override IActionResult HandleDelete(int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            var comment = this.service.Get(id);
            if (this.CanUserDeleteRecord(user, comment))
            {
                return base.HandleDelete(id, user, accessLevel);
            }
            else
            {
                return this.Unauthorized($"{user.DisplayName} cannot edit this comment.");
            }
        }

        protected override IActionResult HandleGet(CommentDTO data, int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, id, user, accessLevel);
        }

        protected override IActionResult HandleGet(IEnumerable<CommentDTO> data, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, user, accessLevel);
        }

        protected override IActionResult HandlePost(CommentDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            var post = this.postService.Get(dataModel.PostId);

            dataModel.Entry.AuthorId = user.Id;
            dataModel.EntryId = post.EntryId;
            dataModel.Entry.Author = user;
            if (CanUserCreateNewRecord(user))
            {
                return base.HandlePost(dataModel, user, accessLevel);
            }
            else
            {
                return this.Unauthorized($"{user.DisplayName} is blocked.");
            }
        }


        protected override IActionResult HandlePut(int id, CommentDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            var comment = this.service.Get(id);
            if (this.CanUserUpdateExistingRecord(user, comment))
            {
                dataModel.Entry.Author = user;
                dataModel.Entry.AuthorId = user.Id;
                dataModel.Entry.Id = comment.Entry.Id;
                dataModel.EntryId = comment.Entry.Id;
                dataModel.Id = comment.Id;
                dataModel.PostId = comment.PostId;
                dataModel.Entry.CreationTime = comment.Entry.CreationTime;
                dataModel.Entry.EditTime = DateTime.Now;
                dataModel.Entry.Likes = comment.Entry.Likes;
                return base.HandlePut(id, dataModel, user, accessLevel);
            }
            else
            {
                return this.Unauthorized($"{user.DisplayName} cannot edit this comment.");
            }
        }


        protected bool CanUserCreateNewRecord(UserDTO user)
        {
            if (user.UserPrivilege.Privilege == UserPrivilegeLevel.Blocked)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected bool CanUserUpdateExistingRecord(UserDTO user, CommentDTO comment)
        {
            if (user.UserPrivilege.Privilege == UserPrivilegeLevel.Admin)
            {
                return true;
            }
            if (comment.WasCreatedBy(user))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override bool CanUserGetAllRecords()
        {
            return true;
        }

        protected bool CanUserDeleteRecord(UserDTO user, CommentDTO comment)
        {
            if (user.UserPrivilege.Privilege == UserPrivilegeLevel.Admin)
            {
                return true;
            }
            if (comment.WasCreatedBy(user))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
