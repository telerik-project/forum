﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.API_Controllers
{
    public class EntriesController : BaseApiController<EntryDTO, EntryWebModel>
    {
        /*
         * Default functionality is handled inside BaseApiController for Get, Get(id), Delete, Post
         * BaseApiController will return a webmodel if the user is not an admin
         * BaseApiController will return a DTO if the user is an admin
         * If you wish to change a function on a controller (Get, Get(id), Delete, Post)
         * Change it here
         * These functions provide you with: The data, the user, their access level (an Enum)
         * If you change these functions - you are responsible for returning an IActionResult appropriately (e.g: Ok, NotFound etc)
         * Calling base.HandleDelete, base.HandleGet, base.HandlePost etc will allow the BaseApiController to perform usual operations
         */

        public EntriesController(IAccessControl accessControl, IEntryService service) : base(accessControl, service) { }

        protected override IActionResult HandlePost(EntryDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandlePost(dataModel, user, accessLevel);
        }

        protected override IActionResult HandleGet(EntryDTO data, int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, id, user, accessLevel);
        }

        protected override IActionResult HandleGet(IEnumerable<EntryDTO> data, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, user, accessLevel);
        }

        protected override IActionResult HandleDelete(int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleDelete(id, user, accessLevel);
        }

        protected override IActionResult HandlePut(int id, EntryDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandlePut(id, dataModel, user, accessLevel);
        }

        protected override bool CanUserCreateNewRecord()
        {
            return true;
        }

        protected override bool CanUserUpdateExistingRecord()
        {
            return true;
        }

        protected override bool CanUserGetAllRecords()
        {
            return true;
        }

        protected override bool CanUserDeleteRecord()
        {
            return true;
        }

    }
}
