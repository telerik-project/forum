﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Web.Models;

namespace Web.API_Controllers
{
    public class LikesController : BaseApiController<LikeDTO, LikeWebModel>
    {
        /*
         * Default functionality is handled inside BaseApiController for Get, Get(id), Delete, Post
         * BaseApiController will return a webmodel if the user is not an admin
         * BaseApiController will return a DTO if the user is an admin
         * If you wish to change a function on a controller (Get, Get(id), Delete, Post)
         * Change it here
         * These functions provide you with: The data, the user, their access level (an Enum)
         * If you change these functions - you are responsible for returning an IActionResult appropriately (e.g: Ok, NotFound etc)
         * Calling base.HandleDelete, base.HandleGet, base.HandlePost etc will allow the BaseApiController to perform usual operations
         */
        private readonly IEntryService entryService;
        public LikesController(IAccessControl accessControl, ILikeService service, IEntryService entryService)
           : base(accessControl, service)
        {
            this.entryService = entryService;
        }

        protected override IActionResult HandleDelete(int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            var like = this.service.Get(id);
            if (this.CanUserDeleteRecord(user,like))
            {
                return base.HandleDelete(id, user, accessLevel);
            }
            else
            {
                return this.Unauthorized($"{user.DisplayName} cannot remove other people's likes");
            }
        }
        protected override IActionResult HandleGet(IEnumerable<LikeDTO> data, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            if (this.CanUserGetAllRecords(user))
            {
                return base.HandleGet(data, user, accessLevel);
            }
            else
            {
                return this.BadRequest($"{user.DisplayName} is not admin.");
            }
        }

        protected override IActionResult HandleGet(LikeDTO data, int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, id, user, accessLevel);
        }

        protected override IActionResult HandlePost(LikeDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            var entry = this.entryService.Get(dataModel.EntryId);
            if (this.CanUserCreateNewRecord(user, entry))
            {
                dataModel.Author = user;
                dataModel.CreationTime = DateTime.Now;
                dataModel.AuthorId = user.Id;
                return base.HandlePost(dataModel, user, accessLevel);
            }
            else
            {
                return this.BadRequest($"{user.DisplayName} already likes this entry.");
            }
            
        }

        protected override IActionResult HandlePut(int id, LikeDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return this.BadRequest("PUT request is not supported for likes.");
        }

        protected bool CanUserCreateNewRecord(UserDTO user, EntryDTO entry)
        {
            foreach (var like in entry.Likes)
            {
                if (like.WasCreatedBy(user))
                {
                    return false;
                }
            }
            return true;
        }

        protected override bool CanUserUpdateExistingRecord()
        {
            return true;
        }

        protected bool CanUserGetAllRecords(UserDTO user)
        {
            if (user.UserPrivilege.Privilege == UserPrivilegeLevel.Admin)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected bool CanUserDeleteRecord(UserDTO user, LikeDTO like)
        {
            if (user.UserPrivilege.Privilege == UserPrivilegeLevel.Admin)
            {
                return true;
            }
            if (like.WasCreatedBy(user))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
