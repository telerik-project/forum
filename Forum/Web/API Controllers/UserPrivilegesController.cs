﻿using Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.API_Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserPrivilegesController : BaseApiController<UserPrivilegeDTO, UserPrivilegeWebModel>
    {
        /*
         * Default functionality is handled inside BaseApiController for Get, Get(id), Delete, Post
         * BaseApiController will return a webmodel if the user is not an admin
         * BaseApiController will return a DTO if the user is an admin
         * If you wish to change a function on a controller (Get, Get(id), Delete, Post)
         * Change it here
         * These functions provide you with: The data, the user, their access level (an Enum)
         * If you change these functions - you are responsible for returning an IActionResult appropriately (e.g: Ok, NotFound etc)
         * Calling base.HandleDelete, base.HandleGet, base.HandlePost etc will allow the BaseApiController to perform usual operations
         */

        public UserPrivilegesController(IAccessControl accessControl, IUserPrivilegeService service) : base(accessControl, service) { }
        protected override IActionResult HandleDelete(int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleDelete(id, user, accessLevel);
        }

        protected override IActionResult HandleGet(IEnumerable<UserPrivilegeDTO> data, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, user, accessLevel);
        }

        protected override IActionResult HandleGet(UserPrivilegeDTO data, int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, id, user, accessLevel);
        }

        protected override IActionResult HandlePost(UserPrivilegeDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandlePost(dataModel, user, accessLevel);
        }

        protected override IActionResult HandlePut(int id, UserPrivilegeDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandlePut(id, dataModel, user, accessLevel);
        }

        protected override bool CanUserCreateNewRecord()
        {
            return false;
        }

        protected override bool CanUserUpdateExistingRecord()
        {
            return false;
        }

        protected override bool CanUserGetAllRecords()
        {
            return true;
        }

        protected override bool CanUserDeleteRecord()
        {
            return false;
        }

    }
}
