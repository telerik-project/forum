﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.API_Controllers
{
    public class PostsController : BaseApiController<PostDTO, PostWebModel>
    {
        /*
         * Default functionality is handled inside BaseApiController for Get, Get(id), Delete, Post
         * BaseApiController will return a webmodel if the user is not an admin
         * BaseApiController will return a DTO if the user is an admin
         * If you wish to change a function on a controller (Get, Get(id), Delete, Post)
         * Change it here
         * These functions provide you with: The data, the user, their access level (an Enum)
         * If you change these functions - you are responsible for returning an IActionResult appropriately (e.g: Ok, NotFound etc)
         * Calling base.HandleDelete, base.HandleGet, base.HandlePost etc will allow the BaseApiController to perform usual operations
         */

        public PostsController(IAccessControl accessControl, IPostService service) : base(accessControl, service) {

        }

        protected override IActionResult HandlePost(PostDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            if (this.CanUserCreateNewRecord(user))
            {
                dataModel.Entry.CreationTime = DateTime.Now;
                dataModel.Entry.AuthorId = user.Id;
                return base.HandlePost(dataModel, user, accessLevel);
            }
            else
            {
                return this.Unauthorized("User is blocked.");
            }
        }

        [HttpGet("filterBy")]
        public IActionResult Get([FromHeaderAttribute] string username, [FromHeaderAttribute] string password, PostSortStrategy sortStrategy, PostFilterStrategy filterStrategy, string filterData)
        {

            IPostService postService = service as IPostService;

            UserDTO user = null;
            UserPrivilegeLevel accessLevel = GetUserPrivilegeLevel(username, password, out user);

            var data = postService.GetAll(sortStrategy,filterStrategy,filterData);

            return HandleGet(data, user, accessLevel);

        }

        protected override IActionResult HandleGet(IEnumerable<PostDTO> data, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, user, accessLevel);
        }

        protected override IActionResult HandleGet(PostDTO data, int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            return base.HandleGet(data, id, user, accessLevel);
        }

        protected override IActionResult HandleDelete(int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            var post = this.service.Get(id);
            if (this.CanUserDeleteRecord(user, post))
            {
                return base.HandleDelete(id, user, accessLevel);
            }
            else
            {
                return this.Unauthorized("User can delete only its own posts");
            }
        }

        protected override IActionResult HandlePut(int id, PostDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {
            var post = this.service.Get(id);
            if (this.CanUserUpdateExistingRecord(user, post))
            {
                dataModel.Entry.CreationTime = post.Entry.CreationTime;
                dataModel.Entry.EditTime = DateTime.Now;
                dataModel.Entry.AuthorId = post.Entry.Author.Id;
                dataModel.Entry.Id = post.EntryId;
                dataModel.EntryId = post.EntryId;
                dataModel.Id = post.Id;
                dataModel.Entry.Author = post.Entry.Author;
                dataModel.Comments = post.Comments;
                dataModel.Entry.Likes = post.Entry.Likes;
                return base.HandlePut(id, dataModel, user, accessLevel);
            }
            else
            {
                return this.BadRequest($"{user.DisplayName} cannot edit post this post");
            }
        }

        protected bool CanUserCreateNewRecord(UserDTO user)
        {
            if (user.UserPrivilege.Privilege == UserPrivilegeLevel.Blocked)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected bool CanUserUpdateExistingRecord(UserDTO user, PostDTO post)
        {
            if (user.UserPrivilege.Privilege == UserPrivilegeLevel.Admin)
            {
                return true;
            }
            if (post.WasCreatedBy(user))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override bool CanUserGetAllRecords()
        {
            return true;
        }

        protected bool CanUserDeleteRecord(UserDTO user, PostDTO post)
        {
            if (user.UserPrivilege.Privilege == UserPrivilegeLevel.Admin)
            {
                return true;
            }
            if (post.WasCreatedBy(user))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}