﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.API_Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseApiController<TDTO, TWebModel> : ControllerBase where 
        TWebModel : BaseWebModel<TDTO>
        where TDTO : IUserCreatedModel
    {

        //To Do: Add authentication
        private readonly IAccessControl accessControl;

        protected readonly IBaseService<TDTO> service;

        public BaseApiController(IAccessControl accessControl, IBaseService<TDTO> service)
        {
            this.service = service;
            this.accessControl = accessControl;
        }

        [HttpGet("")]
        public IActionResult Get([FromHeaderAttribute] string username, [FromHeaderAttribute] string password)
        {

            var data = service.GetAll();

            UserDTO user = null;
            UserPrivilegeLevel accessLevel = GetUserPrivilegeLevel(username, password, out user);

            return HandleGet(data, user, accessLevel);

        }
        protected virtual IActionResult HandleGet(IEnumerable<TDTO> data, UserDTO user, UserPrivilegeLevel accessLevel)
        {

            switch (accessLevel)
            {
                case UserPrivilegeLevel.Admin:
                    {

                        return this.Ok(data);

                    }
                case UserPrivilegeLevel.Blocked:
                case UserPrivilegeLevel.User:
                    {

                        if (CanUserGetAllRecords())
                        {
                            var webData = GetWebModelListFrom(data);

                            return this.Ok(webData);
                        }
                        else
                        {
                            var webData = GetOwnedWebModelListFrom(data, user);

                            return this.Ok(webData);

                        }

                    }
            }

            return this.BadRequest("No dice.");
        }



        [HttpGet("{id}")]
        public IActionResult Get([FromHeaderAttribute] string username, [FromHeaderAttribute] string password, int id)
        {
            TDTO data;
            try
            {
                data = this.service.Get(id);
            }
            catch (ArgumentException error)
            {
                return this.BadRequest(error.Message);
            }

            UserDTO user = null;
            UserPrivilegeLevel accessLevel = GetUserPrivilegeLevel(username, password, out user);

            return HandleGet(data, id, user, accessLevel);

        }

        protected virtual IActionResult HandleGet(TDTO data, int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {

            switch (accessLevel)
            {
                case UserPrivilegeLevel.Admin:
                    {

                        return this.Ok(data);

                    }
                case UserPrivilegeLevel.Blocked:
                case UserPrivilegeLevel.User:
                    {

                        TWebModel webData = (TWebModel)Activator.CreateInstance(typeof(TWebModel), data);

                        return this.Ok(webData);

                    }
            }

            return this.BadRequest("No dice.");

        }

        protected virtual IActionResult HandlePost(TDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {

            switch (accessLevel)
            {
                case UserPrivilegeLevel.Admin:
                    {

                        var data = this.service.Create(dataModel);
                        return this.Created("post", data);

                    }
                case UserPrivilegeLevel.Blocked:
                    {
                        return this.BadRequest("Sorry your account is blocked");
                    }
                case UserPrivilegeLevel.User:
                    {

                        if (CanUserCreateNewRecord())
                        {
                            var data = this.service.Create(dataModel);

                            TWebModel webData = (TWebModel)Activator.CreateInstance(typeof(TWebModel), data);

                            return this.Created("post", webData);
                        }
                        break;
                    }
                case UserPrivilegeLevel.None:
                    {
                        var data = this.service.Create(dataModel);
                        return this.Created("post", data);
                    }
            }

            return this.BadRequest("No dice.");
        }

        [HttpPost("")]
        public IActionResult Post([FromHeaderAttribute] string username, [FromHeaderAttribute] string password, [FromBody] TDTO dataModel)
        {
            
            if (dataModel == null)
            {
                return this.BadRequest();
            }

            UserDTO user = null;
            UserPrivilegeLevel accessLevel = GetUserPrivilegeLevel(username, password, out user);

            return HandlePost(dataModel, user, accessLevel);
        }

        protected virtual IActionResult HandleDelete(int id, UserDTO user, UserPrivilegeLevel accessLevel)
        {

            switch (accessLevel)
            {
                case UserPrivilegeLevel.Admin:
                    {

                        this.service.Delete(id);
                        return this.NoContent();

                    }
                case UserPrivilegeLevel.Blocked:
                    {
                        return this.BadRequest("Sorry your account is blocked");
                    }
                case UserPrivilegeLevel.User:
                    {
                        if (CanUserDeleteRecord())
                        {

                            TDTO data = this.service.Get(id);

                            if (data.WasCreatedBy(user))
                            {

                                this.service.Delete(id);
                                return this.NoContent();

                            }

                        }

                        break;

                    }
            }

            return this.BadRequest("No dice.");
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeaderAttribute] string username, [FromHeaderAttribute] string password, int id)
        {
            UserDTO user = null;
            UserPrivilegeLevel accessLevel = GetUserPrivilegeLevel(username, password, out user);

            return HandleDelete(id, user, accessLevel);

        }

        [HttpPut("{id}")]
        public IActionResult Put([FromHeaderAttribute] string username, [FromHeaderAttribute] string password, int id, [FromBody] TWebModel webModel)
        {
            if (username == null || password == null)
            {
                return this.BadRequest("Invalid credentials.");
            }
            if (webModel == null)
            {
                return this.BadRequest("Invalid input data.");
            }

            UserDTO user = null;
            UserPrivilegeLevel accessLevel = GetUserPrivilegeLevel(username, password, out user);
            
            TDTO dataModel = webModel.ToDTO();

            return HandlePut(id, dataModel, user, accessLevel);
        }

        protected virtual IActionResult HandlePut(int id, TDTO dataModel, UserDTO user, UserPrivilegeLevel accessLevel)
        {

            switch (accessLevel)
            {
                case UserPrivilegeLevel.Admin:
                    {

                        var data = this.service.Update(id,dataModel);
                        return this.Ok(data);

                    }
                case UserPrivilegeLevel.Blocked:
                    {
                        return this.BadRequest("Sorry your account is blocked");
                    }
                case UserPrivilegeLevel.User:
                    {

                        if (CanUserUpdateExistingRecord())
                        {
                            var data = this.service.Get(id);

                            if (data.WasCreatedBy(user))
                            {

                                data = this.service.Update(id, dataModel);

                                return this.Ok(data);
                            }
                            
                        }

                        break;

                    }
            }

            return this.BadRequest("No dice.");
        }

        protected virtual Service.Enums.UserPrivilegeLevel GetUserPrivilegeLevel(string username, string password, out UserDTO user)
        {

            user = null;

            if(username == null && password == null)
            {
                return Service.Enums.UserPrivilegeLevel.None;
            }

            try
            {
                user = this.accessControl.TryGetByUsername(username);
                if (user == null)
                {
                    return Service.Enums.UserPrivilegeLevel.None;
                }
                if (user.Password == PasswordValidator.HashPassword(password))
                {
                    return accessControl.GetUserPrivilegeLevel(username);
                }
                else
                {
                    return Service.Enums.UserPrivilegeLevel.None;
                }
            }
            catch (ArgumentException)
            {
                return Service.Enums.UserPrivilegeLevel.None;
            }

        }

        [NonAction]
        protected virtual bool ValidatePrivileges(string username, string password, out IActionResult result)
        {

            if (username == null || password == null)
            {
                result = this.BadRequest("Invalid login credentials");
                return false;
            }
            try
            {
                var currentUser = this.accessControl.TryGetByUsername(username);
                if (currentUser == null)
                {
                    throw new ArgumentException();
                }
                if (PasswordValidator.HashPassword(currentUser.Password) == password)
                {
                    if (this.accessControl.IsAdmin(currentUser))
                    {
                        result = this.Ok();
                        return true;
                    }
                    else
                    {
                        result = this.Unauthorized($"{currentUser.DisplayName} is not admin.");
                        return false;
                    }
                }
                else
                {
                    result = this.BadRequest("Invalid login credentials");
                    return false;
                }
            }
            catch (ArgumentException)
            {
                result = this.BadRequest("Invalid login credentials");
                return false;
            }
        }

        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public virtual IEnumerable<TWebModel> GetWebModelListFrom(IEnumerable<TDTO> data)
        {

            List<TWebModel> list = new List<TWebModel>();

            foreach (TDTO t in data)
            {
                TWebModel webData = (TWebModel)Activator.CreateInstance(typeof(TWebModel), t);

                list.Add(webData);
            }

            return list;

        }

        [NonAction]
        public virtual IEnumerable<TWebModel> GetOwnedWebModelListFrom(IEnumerable<TDTO> data, UserDTO user)
        {

            List<TWebModel> list = new List<TWebModel>();

            foreach (TDTO t in data)
            {

                if (t.WasCreatedBy(user))
                {

                    TWebModel webData = (TWebModel)Activator.CreateInstance(typeof(TWebModel), t);

                    list.Add(webData);

                }

            }

            return list;

        }

        //Access Control
        protected virtual bool CanUserCreateNewRecord()
        {
            return true;
        }

        protected virtual bool CanUserUpdateExistingRecord()
        {
            return true;
        }

        protected virtual bool CanUserGetAllRecords()
        {
            return true;
        }

        protected virtual bool CanUserDeleteRecord()
        {
            return true;
        }


    }
}
