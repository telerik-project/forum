﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public abstract class BaseWebModel<T>
    {

        public abstract T ToDTO();

        protected abstract void FromDTO(T data);

        public BaseWebModel() { }
        public BaseWebModel(T data)
        {
            FromDTO(data);
        }
    }
}
