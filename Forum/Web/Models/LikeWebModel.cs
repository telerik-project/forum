﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class LikeWebModel : BaseWebModel<LikeDTO>
    {
        public LikeWebModel() : base() { }
        public LikeWebModel(LikeDTO dto) : base(dto) { }
        public UserWebModel Author { get; set; }
        public DateTime CreationTime { get; set; }
        public int LikeId { get; set; }

        public override LikeDTO ToDTO()
        {
            return new LikeDTO()
            {
                //Author = this.Author.ToDTO(),
                CreationTime = this.CreationTime
            };
        }

        protected override void FromDTO(LikeDTO data)
        {
            this.LikeId = data.Id;
            this.Author = new UserWebModel(data.Author);
            this.CreationTime = data.CreationTime;
        }
    }
}
