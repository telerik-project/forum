﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class CommentWebModel : BaseWebModel<CommentDTO>
    {

        public EntryWebModel Entry { get; set; }
        public int EntryId { get; set; }
        public int CommentId { get; set; }

        public CommentWebModel() : base() { }
        public CommentWebModel(CommentDTO dto) : base(dto) { }
        
        protected override void FromDTO(CommentDTO data)
        {
            CommentId = data.Id;
            EntryId = data.EntryId;
            Entry = new EntryWebModel(data.Entry);
        }

        public override CommentDTO ToDTO()
        {
            return new CommentDTO
            {
                EntryId = this.EntryId,
                Entry = this.Entry.ToDTO()
            };
        }
    }
}
