﻿using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class UserPrivilegeWebModel : BaseWebModel<UserPrivilegeDTO>
    {

        public UserPrivilegeWebModel() : base() { }
        public UserPrivilegeWebModel(UserPrivilegeDTO dto) : base(dto) { }

        public UserPrivilegeLevel Privilege { get; set; }

        public override UserPrivilegeDTO ToDTO()
        {
            return new UserPrivilegeDTO
            {
                Privilege = this.Privilege
            };
        }

        protected override void FromDTO(UserPrivilegeDTO data)
        {
            Privilege = data.Privilege;
        }
    }
}
