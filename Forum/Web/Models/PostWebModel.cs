﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class PostWebModel : BaseWebModel<PostDTO>
    {

        public EntryWebModel Entry { get; set; }
        public string Title { get; set; }
        public int PostId { get; set; }
        public List<CommentWebModel> Comments { get; set; }

        public PostWebModel() : base() { }
        public PostWebModel(PostDTO dto) : base(dto) { }
        public override PostDTO ToDTO()
        {

            List<CommentDTO> comments = new List<CommentDTO>();
            if (Comments != null)
            {
                for (int i = 0; i < Comments.Count; i++)
                {
                    comments.Add(Comments[i].ToDTO());
                }
            }
            

            return new PostDTO
            {
                Entry = this.Entry.ToDTO(),
                Title = this.Title,
                Comments = comments
            };
        }

        protected override void FromDTO(PostDTO data)
        {
            PostId = data.Id;
            Title = data.Title;
            Entry = new EntryWebModel(data.Entry);

            this.Comments = new List<CommentWebModel>();
            for(int i = 0; i < data.Comments.Count; i++)
            {
                this.Comments.Add(new CommentWebModel(data.Comments[i]));
            }

        }
    }
}
