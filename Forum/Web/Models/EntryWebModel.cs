﻿using Data.Models;
using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class EntryWebModel : BaseWebModel<EntryDTO>
    {
        public EntryWebModel() : base() { }
        public EntryWebModel(EntryDTO dto) : base(dto) { }
        public UserWebModel Author { get; set; }
        public string Content { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime EditTime { get; set; }
        public List<LikeWebModel> Likes { get; set; }

        //public int LikesCount { get; set; } //short version just displaying how many likes as count
        public override EntryDTO ToDTO()
        {
            return new EntryDTO()
            {
                //Author = this.Author.ToDTO(),
                Content = this.Content,
                CreationTime = this.CreationTime,
                EditTime = this.EditTime
            };
        }

        protected override void FromDTO(EntryDTO data)
        {
            this.Author = new UserWebModel(data.Author);
            this.Content = data.Content;
            this.CreationTime = data.CreationTime;
            this.EditTime = data.EditTime;

            //this.LikesCount = data.Likes.Count(); //short version just displaying how many likes as count

            var webModelLikes = new List<LikeWebModel>();

            foreach (var like in data.Likes)
            {
                webModelLikes.Add(new LikeWebModel(like));
            }

            this.Likes = webModelLikes;
        }
    }
}
