﻿using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class UserWebModel : BaseWebModel<UserDTO>
    {

        //We need to decide what we allow the web model to see......
        //Should it see password? Probably not
        public UserWebModel() : base() { }

        public string Username { private get; set; }
        public string DisplayName { get; set; }

        public string Email { private get; set; }
        public string Password { private get; set; }
        public string ProfilePicture { get; set; }

        public UserPrivilegeWebModel UserPrivilege { get; set; }

        public UserWebModel(UserDTO dto) : base(dto) { }

        public override UserDTO ToDTO()
        {
            return new UserDTO
            {
                DisplayName = this.DisplayName,
                Email = this.Email,
                Password = this.Password,
                Username = this.Username,
                UserPrivilege = UserPrivilege.ToDTO(),
                ProfilePicture = this.ProfilePicture
            };
        }

        protected override void FromDTO(UserDTO data)
        {
            this.Username = data.Username;          
            this.UserPrivilege = new UserPrivilegeWebModel(data.UserPrivilege);
            this.Email = data.Email;
            this.Password = data.Password;
            this.DisplayName = data.DisplayName;
            this.ProfilePicture = data.ProfilePicture;

        }
    }
}
