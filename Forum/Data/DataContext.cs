﻿using Data.Contracts;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Entry> Entries { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserPrivilege> UserPrivileges { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Table Relations
            modelBuilder.Entity<Entry>().HasOne(w => w.Author).WithMany()
                .HasForeignKey(s => s.AuthorId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Entry>().HasMany(w => w.Likes).WithOne().OnDelete(DeleteBehavior.NoAction);

            /*modelBuilder.Entity<Comment>().HasOne(w => w.Entry).WithMany()
                .HasForeignKey(s => s.EntryId)
                .OnDelete(DeleteBehavior.NoAction);*/

            modelBuilder.Entity<Comment>().HasOne(w => w.Entry).WithOne()
                .OnDelete(DeleteBehavior.NoAction);

            /*modelBuilder.Entity<Post>().HasOne(w => w.Entry).WithMany()
                .HasForeignKey(s => s.EntryId)
                .OnDelete(DeleteBehavior.NoAction);*/

            modelBuilder.Entity<Post>().HasOne(w => w.Entry).WithOne()
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>()
                .HasOne<UserPrivilege>(s => s.UserPrivilege)
                .WithOne(ad => ad.User)
                .HasForeignKey<UserPrivilege>(ad => ad.UserId);

            #endregion
            //And then seed the data
            modelBuilder.Seed();
        }

        void IDataContext.SaveChanges()
        {
            SaveChanges();
        }
    }
}
