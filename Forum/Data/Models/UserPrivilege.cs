﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class UserPrivilege : BaseEntity
    {

        public int UserId { get; set; }

        public User User { get; set; }

        public int Privilege { get; set; }

        public override void Update(BaseEntity e)
        {

            UserPrivilege data = e as UserPrivilege;

            Privilege = data.Privilege;

        }

    }
}
