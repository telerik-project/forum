﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Entry : BaseEntity
    {
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public string Content { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime EditTime { get; set; }
        
        //And a list of likes
        public List<Like> Likes { get; set; }

        public override void Update(BaseEntity e)
        {

            Entry data = e as Entry;

            Content = data.Content;
            EditTime = DateTime.Now;

        }

    }
}
