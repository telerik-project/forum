﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Comment : BaseEntity
    {
        public int PostId { get; set; }
        public int EntryId { get; set; }
        public Entry Entry { get; set;}

        public override void Update(BaseEntity e)
        {

            Comment data = e as Comment;

            Entry.Update(data.Entry);

        }
    }
}
