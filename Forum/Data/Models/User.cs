﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class User : BaseEntity
    {
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        //public int UserPrivilegeId { get; set; }
        public UserPrivilege UserPrivilege { get; set; }
        public string ProfilePicture { get; set; }
        public override void Update(BaseEntity e)
        {
            User data = e as User;

            Username = data.Username;
            DisplayName = data.DisplayName;
            Email = data.Email;

            UserPrivilege.Update(data.UserPrivilege);

        }
    }
}
