﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Post : BaseEntity
    {
        public List<Comment> Comments { get; set; }
        public string Title { get; set; }
        public int EntryId { get; set; }
        public Entry Entry { get; set; }

        public override void Update(BaseEntity e)
        {

            Post data = e as Post;

            Title = data.Title;
            Entry.Update(data.Entry);

        }
    }
}
