﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Like : BaseEntity
    {
        public int EntryId { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public DateTime CreationTime { get; set; }

        public override void Update(BaseEntity e)
        {

            //There's nothing to update about a like

        }
    }
}
