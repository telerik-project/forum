﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public static class SeedModels
    {
        public static List<User> GetUsers()
        {
            return new List<User>()
            {
                new User()
                {
                    DisplayName = "ADMIN",
                    Email = "admin@forum.com",
                    Id = 1,
                    Username = "admin",
                    Password = "jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=",
                    ProfilePicture = "~/Images/cat.png"
                },
                new User()
                {
                    DisplayName = "USER",
                    Email = "user@forum.com",
                    Id = 2,
                    Username = "user",
                    Password = "BPiZbadjt6lpsQKO4wB1aerzpjVIbdqyEdUSyFud+Ps=",
                    ProfilePicture = "~/Images/default_picture.png"
                },
                new User()
                {
                    DisplayName = "BLOCKED USER",
                    Email = "blocked@forever.com",
                    Id = 3,
                    Username = "blocked",
                    Password = "aXPd3T75y2opMnAvMXd/qtnJvzEk0UeoTzGq220TlUY=",
                    ProfilePicture = "~/Images/default_picture.png"
                }
            };
        }
        public static List<UserPrivilege> GetPrivileges()
        {
            return new List<UserPrivilege>()
            {
                new UserPrivilege()
                {
                    Id = 1,
                    UserId = 1,
                    Privilege = 1
                },
                new UserPrivilege()
                {
                    Id = 2,
                    UserId = 2,
                    Privilege = 2
                },
                new UserPrivilege()
                {
                    Id = 3,
                    UserId = 3,
                    Privilege = 3
                }
            };
        }
        public static List<Comment> GetComments()
        {
            return new List<Comment>()
            {
                new Comment()
                {
                    EntryId = 12,
                    Id = 1,
                    PostId = 6,
                },
                new Comment()
                {
                    EntryId = 13,
                    Id = 2,
                    PostId = 7,
                },
                new Comment()
                {
                    EntryId = 14,
                    Id = 3,
                    PostId = 8,
                },
                new Comment()
                {
                    EntryId = 15,
                    Id = 4,
                    PostId = 9,
                },
                new Comment()
                {
                    EntryId = 16,
                    Id = 5,
                    PostId = 10,
                },
                /**/
                new Comment()
                {
                    EntryId = 17,
                    Id = 6,
                    PostId = 1,
                },
                new Comment()
                {
                    EntryId = 18,
                    Id = 7,
                    PostId = 2,
                },
                new Comment()
                {
                    EntryId = 19,
                    Id = 8,
                    PostId = 3,
                },
                new Comment()
                {
                    EntryId = 20,
                    Id = 9,
                    PostId = 4,
                },
                new Comment()
                {
                    EntryId = 21,
                    Id = 10,
                    PostId = 5
                }
            };
        }
        public static List<Entry> GetEntries()
        {
            return new List<Entry>()
            {
                new Entry()
                {
                    Id = 1,
                    AuthorId = 1,
                    Content = "first entry - post - by admin",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 2,
                    AuthorId = 1,
                    Content = "second entry - post - by admin",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 3,
                    AuthorId = 1,
                    Content = "third entry - post - by admin",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 4,
                    AuthorId = 1,
                    Content = "fourth entry - post - by admin",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 5,
                    AuthorId = 1,
                    Content = "fifth entry - post - by admin",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 6,
                    AuthorId = 2,
                    Content = "sixth entry - post - by user",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 7,
                    AuthorId = 2,
                    Content = "seventh entry - post - by user",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 8,
                    AuthorId = 2,
                    Content = "eight entry - post - by user",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 9,
                    AuthorId = 2,
                    Content = "ninth entry - post - by user",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 10,
                    AuthorId = 2,
                    Content = "tenth entry - post - by user",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 11,
                    AuthorId = 3,
                    Content = "eleventh entry - post - by BLOCKED user",
                    CreationTime = Format(DateTime.Now).AddHours(1),
                    EditTime = Format(DateTime.Now).AddHours(1)
                },//11 post by blocked user
                new Entry()
                {
                    Id = 12,
                    AuthorId = 1,
                    Content = "twelveth entry - comment - by admin",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 13,
                    AuthorId = 1,
                    Content = "thirdteenth entry - comment - by admin",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 14,
                    AuthorId = 1,
                    Content = "fourteenth entry - comment - by admin",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 15,
                    AuthorId = 1,
                    Content = "fifteenth entry - comment - by admin",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 16,
                    AuthorId = 1,
                    Content = "sixteenth entry - comment - by admin",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 17,
                    AuthorId = 2,
                    Content = "seventeenth entry - comment - by user",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 18,
                    AuthorId = 2,
                    Content = "eithteenth entry - comment - by user",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 19,
                    AuthorId = 2,
                    Content = "nineteenth entry - comment - by user",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 20,
                    AuthorId = 2,
                    Content = "twentieth entry - comment - by user",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                },
                new Entry()
                {
                    Id = 21,
                    AuthorId = 2,
                    Content = "twentieth first entry - comment - by user",
                    CreationTime = Format(DateTime.Now),
                    EditTime = Format(DateTime.Now)
                }
            };
        }
        public static List<Post> GetPosts()
        {
            return new List<Post>()
            {
                new Post()
                {
                    EntryId = 1,
                    Id = 1,
                    Title = "1st post - by admin"
                },
                new Post()
                {
                    EntryId = 2,
                    Id = 2,
                    Title = "2nd post - by admin"
                },
                new Post()
                {
                    EntryId = 3,
                    Id = 3,
                    Title = "3rd post - by admin"
                },
                new Post()
                {
                    EntryId = 4,
                    Id = 4,
                    Title = "4th post - by admin"
                },
                new Post()
                {
                    EntryId = 5,
                    Id = 5,
                    Title = "5th post - by admin"
                },
                new Post()
                {
                    EntryId = 6,
                    Id = 6,
                    Title = "6th post - by user"
                },
                new Post()
                {
                    EntryId = 7,
                    Id = 7,
                    Title = "7th post - by user"
                },
                new Post()
                {
                    EntryId = 8,
                    Id = 8,
                    Title = "8th post - by user"
                },
                new Post()
                {
                    EntryId = 9,
                    Id = 9,
                    Title = "9th post - by user"
                },
                new Post()
                {
                    EntryId = 10,
                    Id = 10,
                    Title = "10th post - by user"
                },
                new Post()
                {
                    EntryId = 11,
                    Id = 11,
                    Title = "11th post - by BLOCKED user"
                },
            };
        }
        public static List<Like> GetLikes()
        {
            return new List<Like>()
            {
                new Like()
                {
                    AuthorId = 2,
                    Id = 1,
                    EntryId = 1,
                    CreationTime = Format(DateTime.Now)
                },
                new Like()
                {
                    AuthorId = 2,
                    Id = 2,
                    EntryId = 2,
                    CreationTime = Format(DateTime.Now)
                },
                new Like()
                {
                    AuthorId = 2,
                    Id = 3,
                    EntryId = 3,
                    CreationTime = Format(DateTime.Now)
                },
                new Like()
                {
                    AuthorId = 2,
                    Id = 4,
                    EntryId = 4,
                    CreationTime = Format(DateTime.Now)
                },
                new Like()
                {
                    AuthorId = 2,
                    Id = 5,
                    EntryId = 5,
                    CreationTime = Format(DateTime.Now)
                },
                new Like()
                {
                    AuthorId = 1,
                    Id = 6,
                    EntryId = 6,
                    CreationTime = Format(DateTime.Now)
                },
                new Like()
                {
                    AuthorId = 1,
                    Id = 7,
                    EntryId = 7,
                    CreationTime = Format(DateTime.Now)
                },
                new Like()
                {
                    AuthorId = 1,
                    Id = 8,
                    EntryId = 8,
                    CreationTime = Format(DateTime.Now)
                },
                new Like()
                {
                    AuthorId = 1,
                    Id = 9,
                    EntryId = 9,
                    CreationTime = Format(DateTime.Now)
                },
                new Like()
                {
                    AuthorId = 1,
                    Id = 10,
                    EntryId = 10,
                    CreationTime = Format(DateTime.Now)
                }
            };
        }
        private static DateTime Format(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Kind);
        }
    }
}
