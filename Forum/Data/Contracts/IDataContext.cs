﻿using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Contracts
{
    public interface IDataContext
    {
        DbSet<Comment> Comments { get; }
        DbSet<Entry> Entries { get; }
        DbSet<Like> Likes { get; }
        DbSet<Post> Posts { get; }
        DbSet<User> Users { get; }

        DbSet<UserPrivilege> UserPrivileges { get; }

        void SaveChanges();
    }
}
