﻿using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public static class ModelBuilderExtensions
    {
        //NOTE TO SELF: WE WILL NEED A FILE TO STORE CONSTANTS FOR USER PRIVILEGES

        public static IEnumerable<Entry> entries;
        public static IEnumerable<Comment> comments;
        public static IEnumerable<Post> posts;
        public static IEnumerable<Like> likes;
        public static IEnumerable<User> users;
        public static IEnumerable<UserPrivilege> privileges;
        static ModelBuilderExtensions()
        {
            
            users = SeedModels.GetUsers();
            privileges = SeedModels.GetPrivileges();
            entries = SeedModels.GetEntries();
            posts = SeedModels.GetPosts();
            likes = SeedModels.GetLikes();
            comments = SeedModels.GetComments();
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserPrivilege>().HasData(privileges);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Entry>().HasData(entries);
            modelBuilder.Entity<Post>().HasData(posts);
            modelBuilder.Entity<Comment>().HasData(comments);
            modelBuilder.Entity<Like>().HasData(likes);
        }
    }
}
