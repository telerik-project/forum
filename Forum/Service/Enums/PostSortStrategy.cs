﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Enums
{
    public enum PostSortStrategy
    {
        None,
        Date_Descending,
        Date_Ascending,
        Likes_Descending,
        Likes_Ascending,
        Comments_Descending,
        Comments_Ascending
    }
}
