﻿using Data.Contracts;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class PostService : BaseService<Post, PostDTO>, IPostService
    {

        public PostService(IDataContext database) : base(database) { }

        public override void AddToDbContext(Post data)
        {
            this.dbContext.Posts.Add(data);
            dbContext.SaveChanges();
        }

        public IEnumerable<PostDTO> GetAll(PostSortStrategy sortStrategy = PostSortStrategy.None, PostFilterStrategy filterStrategy = PostFilterStrategy.None, string filterData = "", int fromPost = -1, int entries = -1)
        {
            IQueryable<Post> allData = GetAllData();

            allData = FilterDeletedData(allData);

            IQueryable<Post> returnData = allData;

            switch (filterStrategy)
            {

                case PostFilterStrategy.DisplayName:
                    {

                        returnData = returnData.Where(x => x.Entry.Author.DisplayName.ToLower() == filterData.ToLower());

                        break;
                    }
                case PostFilterStrategy.UserName:
                    {

                        returnData = returnData.Where(x => x.Entry.Author.Username.ToLower() == filterData.ToLower());

                        break;
                    }
                case PostFilterStrategy.Email:
                    {

                        returnData = returnData.Where(x => x.Entry.Author.Email.ToLower() == filterData.ToLower());

                        break;
                    }
                case PostFilterStrategy.None:
                    {
                        break;
                    }
            }

            switch (sortStrategy)
            {
                case PostSortStrategy.Date_Ascending:

                    returnData = returnData.OrderBy(x => x.Entry.CreationTime);

                    break;
                case PostSortStrategy.None:
                case PostSortStrategy.Date_Descending:

                    returnData = returnData.OrderByDescending(x => x.Entry.CreationTime);

                    break;
                case PostSortStrategy.Likes_Descending:

                    returnData = returnData.OrderByDescending(x => 
                        x.Entry.Likes.Where(x=>!x.IsDeleted)
                    .Count());

                    break;
                case PostSortStrategy.Likes_Ascending:

                    returnData = returnData.OrderBy(x =>
                        x.Entry.Likes.Where(x => !x.IsDeleted)
                    .Count());

                    break;
                case PostSortStrategy.Comments_Descending:

                    returnData = returnData.OrderByDescending(x =>
                        x.Comments.Where(x => !x.IsDeleted)
                    .Count());

                    break;
                case PostSortStrategy.Comments_Ascending:

                    returnData = returnData.OrderBy(x =>
                        x.Comments.Where(x => !x.IsDeleted)
                    .Count());

                    break;
            }

            if (fromPost >= 0 && entries > 0)
            {
                returnData = returnData.Skip(fromPost).Take(entries);
            }

            IEnumerable<PostDTO> d = GetDTOListFrom(returnData);

            return d;

        }

        protected override IQueryable<Post> GetAllData()
        {
            return this.dbContext.Posts.Include(x => x.Entry)
                .ThenInclude(x => x.Author)
                .ThenInclude(x => x.UserPrivilege)
                .Include(x => x.Comments)
                .ThenInclude(x => x.Entry)
                .ThenInclude(x => x.Author)
                .ThenInclude(x => x.UserPrivilege)
                .Include(x => x.Entry)
                .ThenInclude(x => x.Likes)
                .ThenInclude(x => x.Author)
                .ThenInclude(x => x.UserPrivilege)
                .Include(x => x.Comments)
                .ThenInclude(x => x.Entry)
                .ThenInclude(x => x.Likes)
                .ThenInclude(x => x.Author)
                .ThenInclude(x => x.UserPrivilege);
        }

        protected override DbSet<Post> GetDataSet()
        {
            return this.dbContext.Posts;
        }
        public int GetAllByUser(int id)
        {
            return this.dbContext.Posts.Where(x => x.Entry.AuthorId == id).Count();
        }
    }
}
