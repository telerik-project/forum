﻿using Data.Contracts;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Service.DtoModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public abstract class BaseService<TData, TDTO> 
        where TData : BaseEntity
        where TDTO : BaseDTO<TData>, new()
    {

        /*
         * The default base class for all services
         * This provides basic Get, Create, Delete, Update functionality
         * For functionality and business logic that is specific to a certain data class,
         * Put this in an overrided or new function in the relevant service e.g: If you want to add "Get Like Count", 
         * add it in LikeService
         */

        protected readonly IDataContext dbContext;

        public BaseService(IDataContext database){
            
            this.dbContext = database;
        }

        public int GetRecordCount()
        {
            return GetAllData().Where(x => x.IsDeleted == false).Count();
        }

        protected abstract IQueryable<TData> GetAllData();

        protected IQueryable<TData> FilterDeletedData(IQueryable<TData> data)
        {
            return data.Where(x => x.IsDeleted == false);
        }

        protected abstract DbSet<TData> GetDataSet();


        public virtual TDTO Get(int id)
        {
            TData t = this.GetAllData().Where(x => x.IsDeleted == false).FirstOrDefault<TData>(x => x.Id == id);

            //Now we want to transform this data model into a DTO so we can return it (we don't want data models going directly to web)

            //Because we can't create a templated type with a 'new' keyword with arguments
            //I found this code online instead

            if(t == null)
            {
                throw new ArgumentException("No such data for: " + id);
            }

            //Here we create a new DTO - using the constructor in the DTO base class that takes a data model as an argument
            TDTO dto = (TDTO)Activator.CreateInstance(typeof(TDTO), t);

            return dto;

        }

        public virtual IEnumerable<TDTO> GetDTOListFrom(IEnumerable<TData> data)
        {
            //This function will take a list of data models and return a list of DTO

            //Create a new list...
            List<TDTO> list = new List<TDTO>();

            foreach(TData t in data)
            {
                if (t.IsDeleted)
                {
                    continue;
                }

                //Create a DTO from each data model in the list
                TDTO dto = (TDTO)Activator.CreateInstance(typeof(TDTO), t);

                //Add it to the list...
                list.Add(dto);
            }

            return list;

        }

        public virtual IEnumerable<TDTO> GetAll()
        {

            //FIrst we get the data set we want to deal with from the datacontext
            IEnumerable<TData> data = GetAllData().Where(x => x.IsDeleted == false);

            //And then we call our handy GetDTOList function...
            IEnumerable<TDTO> dto = GetDTOListFrom(data);

            return dto;

        }

        public virtual TDTO Update(int id, TDTO dto)
        {

            IEnumerable<TData> data = GetAllData();

            TData newData = dto.ToData();

            TData t = data.FirstOrDefault<TData>(x => x.Id == id);

            t.Update(newData);

            this.dbContext.SaveChanges();

            return Get(t.Id);

        }

        public virtual TDTO Create(TDTO dto)
        {
            
            TData data = dto.ToData();

            this.AddToDbContext(data);

            this.dbContext.SaveChanges();

            dto = (TDTO)Activator.CreateInstance(typeof(TDTO), data);

            return dto;

        }

        public abstract void AddToDbContext(TData data);

        public virtual void Delete(int id)
        {

            DbSet<TData> dataSet = GetDataSet();

            TData t = dataSet.FirstOrDefault<TData>(x => x.Id == id);
            try
            {
                t.IsDeleted = true;
                //dataSet.Remove(t);
            }
            catch (ArgumentNullException)
            {
                throw new NullReferenceException();
            }

            this.dbContext.SaveChanges();

        }

    }
}
