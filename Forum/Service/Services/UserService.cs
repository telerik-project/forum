﻿using Data.Contracts;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class UserService : BaseService<User, UserDTO>, IUserService
    {

        public ILikeService likeService;
        public IPostService postService;
        public ICommentService commentService;

        public UserService(IDataContext database, ILikeService likeService, IPostService postService, ICommentService commentService) : base(database) {

            this.likeService = likeService;
            this.postService = postService;
            this.commentService = commentService;
        
        }

        public void PopulateUserDTO(UserDTO dto)
        {

            dto.LikeCount = this.likeService.GetAllByUser(dto.Id);
            dto.PostCount = this.postService.GetAllByUser(dto.Id);
            dto.CommentCount = this.commentService.GetAllByUser(dto.Id);

        }

        public UserDTO GetByUsername(string username)
        {
            User u = GetAllData()
                .FirstOrDefault(x => x.Username == username);
            if (u == null)
            {
                throw new ArgumentException();
            }

            UserDTO dto = new UserDTO(u);

            PopulateUserDTO(dto);

            return dto;
        }

        public UserDTO AuthenticateUser(string username, string password)
        {
            var hashed = PasswordValidator.HashPassword(password);

            User u = GetAllData()
                .FirstOrDefault(x => x.Username == username && x.Password == hashed);
            if (u == null)
            {
                throw new ArgumentException();
            }

            UserDTO dto = new UserDTO(u);


            PopulateUserDTO(dto);

            return dto;
        }

        public IEnumerable<UserDTO> GetAll(UserFilterStrategy filterStrategy = UserFilterStrategy.None, string filterInput = "", int fromEntry = -1, int entries = -1)
        {
            
            IQueryable<User> users = GetAllData();

            users = FilterDeletedData(users);

            switch (filterStrategy)
            {
                case UserFilterStrategy.UserName:
                {
                    users = users.Where(x => x.Username.ToLower() == filterInput.ToLower());
                    break;
                }
                case UserFilterStrategy.DisplayName:
                {
                    users = users.Where(x => x.DisplayName.ToLower() == filterInput.ToLower());
                    break;
                }
                case UserFilterStrategy.Email:
                {
                    users = users.Where(x => x.Email.ToLower() == filterInput.ToLower());
                    break;
                }
                case UserFilterStrategy.None:
                {
                        break;
                }
                case UserFilterStrategy.All:
                {
                    users = users.Where(x => x.Email.ToLower().Contains(filterInput.ToLower()) || x.Username.ToLower().Contains(filterInput.ToLower()) || x.DisplayName.ToLower().Contains(filterInput.ToLower()));
                    break;
                }
            }

            if (fromEntry >= 0 && entries > 0)
            {
                users = users.Skip(fromEntry).Take(entries);
            }

            IEnumerable<UserDTO> userDTOs = this.GetDTOListFrom(users);

            foreach(UserDTO dto in userDTOs)
            {
                PopulateUserDTO(dto);
            }

            return userDTOs;

        }

        protected override IQueryable<User> GetAllData()
        {
            return this.dbContext.Users.Include(x => x.UserPrivilege).ThenInclude(x => x.User);
        }

        protected override DbSet<User> GetDataSet()
        {
            return this.dbContext.Users;
        }

        public override void AddToDbContext(User data)
        {
            this.CheckForAvailability(data.Username, data.Email, data.DisplayName);
            this.dbContext.Users.Add(data);
        }

        public bool CheckForAvailability(string username, string email, string displayName)
        {
            foreach (var user in dbContext.Users)
            {
                if (user.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new ArgumentException("Username already taken.");
                }
                if (user.DisplayName.Equals(displayName, StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new ArgumentException("Display name already taken.");
                }
                if (user.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new ArgumentException("Email already taken.");
                }
            }
            return true;
        }

        public User SetProfilePicture(string username, string relativePath)
        {
            var user = this.dbContext.Users.FirstOrDefault(x => x.Username == username);
            user.ProfilePicture = relativePath;
            dbContext.SaveChanges();
            return user;
        }

        public override UserDTO Create(UserDTO dto)
        {
            User user = dto.ToData();
            if (user.UserPrivilege == null || user.UserPrivilege.Privilege == 0)
            {
                user.UserPrivilege = new UserPrivilege()
                {
                    Privilege = 2,
                };
            }
            user.ProfilePicture = "~/Images/default_picture.png";
            this.AddToDbContext(user);

            this.dbContext.SaveChanges();

            dto = (UserDTO)Activator.CreateInstance(typeof(UserDTO), user);

            return dto;
        }

        public void BlockUser(int id)
        {
            User user = dbContext.Users.Include(x => x.UserPrivilege).FirstOrDefault(x => x.Id == id);
            if (user == null)
            {
                throw new ArgumentException($"There is no user with ID {id}");
            }
            if (user.UserPrivilege.Privilege == (int)UserPrivilegeLevel.Blocked)
            {
                throw new ArgumentException($"User with ID {id} is already blocked.");
            }
            user.UserPrivilege.Privilege = (int)UserPrivilegeLevel.Blocked;
            dbContext.SaveChanges();
        }

        public void UnblockUser(int id)
        {
            User user = dbContext.Users.Include(x => x.UserPrivilege).FirstOrDefault(x => x.Id == id);
            if (user == null)
            {
                throw new ArgumentException($"There is no user with ID {id}");
            }
            user.UserPrivilege.Privilege = (int)UserPrivilegeLevel.User;
            dbContext.SaveChanges();
        }
    }
}
