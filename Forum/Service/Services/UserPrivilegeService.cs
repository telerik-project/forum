﻿using Data.Contracts;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Service.Contracts;
using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class UserPrivilegeService : BaseService<UserPrivilege, UserPrivilegeDTO>, IUserPrivilegeService
    {

        public UserPrivilegeService(IDataContext database) : base(database) { }


        //This returns the DBSet - we use this for adding, deleting and updating
        protected override DbSet<UserPrivilege> GetDataSet()
        {
            return this.dbContext.UserPrivileges;
        }

        //This returns the data itself - with the Includes - it is for reading only
        protected override IQueryable<UserPrivilege> GetAllData()
        {
               return this.dbContext.UserPrivileges.Include(x => x.User);
        }

        public override void AddToDbContext(UserPrivilege data)
        {
            this.dbContext.UserPrivileges.Add(data);
        }
    }
}
