﻿using Data.Models;
using Service.Contracts;
using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class AccessControl : IAccessControl
    {
        private readonly IUserService userService;
        public AccessControl(IUserService userService)
        {
            this.userService = userService;
        }

        public UserDTO TryGetByUsername(string username)
        {
            try
            {
                var user = this.userService.GetByUsername(username);
                return user;
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Username not found.");
            }
        }

        public Service.Enums.UserPrivilegeLevel GetUserPrivilegeLevel(string username)
        {
            
            UserDTO u = TryGetByUsername(username);

            if(u != null)
            {
                return (Service.Enums.UserPrivilegeLevel)u.UserPrivilege.Privilege;
            }

            return Enums.UserPrivilegeLevel.None;

        }

        public bool IsAdmin(UserDTO user)
        {
            return user.UserPrivilege.Privilege == Service.Enums.UserPrivilegeLevel.Admin;
        }
    }
}
