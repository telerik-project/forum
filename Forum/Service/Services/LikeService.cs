﻿using Data.Contracts;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Service.Contracts;
using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class LikeService : BaseService<Like, LikeDTO>, ILikeService
    {
        public LikeService(IDataContext database) : base(database) { }

        protected override IQueryable<Like> GetAllData()
        {
            return dbContext.Likes
                .Include(x => x.Author).ThenInclude(x => x.UserPrivilege);
                /*.Include(x => x.Entry).ThenInclude(x => x.Author).ThenInclude(x => x.UserPrivilege);*/
        }

        protected override DbSet<Like> GetDataSet()
        {
            return dbContext.Likes;
        }

        public override void AddToDbContext(Like data)
        {
            this.dbContext.Likes.Add(data);
        }
        public int GetAllByUser(int id)
        {
            return this.dbContext.Likes.Where(x => x.AuthorId == id).Count();
        }
    }
}
