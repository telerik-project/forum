﻿using Data.Contracts;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Service.Contracts;
using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class CommentService : BaseService<Comment, CommentDTO>, ICommentService
    {

        public CommentService(IDataContext database) : base(database) { }

        public override void AddToDbContext(Comment data)
        {
            this.dbContext.Comments.Add(data);
            dbContext.SaveChanges();
        }

        protected override IQueryable<Comment> GetAllData()
        {
            IQueryable < Comment > res = this.dbContext.Comments
                .Include(x => x.Entry)
                .ThenInclude(x => x.Author)
                .ThenInclude(x => x.UserPrivilege)
                .Include(x => x.Entry)
                .ThenInclude(x => x.Likes)
                .ThenInclude(x => x.Author)
                .ThenInclude(x => x.UserPrivilege);

            return res;

        }

        public int GetAllByUser(int id)
        {
            return this.dbContext.Comments.Where(x => x.Entry.AuthorId == id).Count();
        }

        public IEnumerable<CommentDTO> GetAll(CommentSortStrategy sortStrategy = CommentSortStrategy.None, int fromEntry = -1, int entries = -1)
        {
            IQueryable<Comment> allData = GetAllData();

            allData = FilterDeletedData(allData);

            switch (sortStrategy)
            {
                case CommentSortStrategy.None:
                case CommentSortStrategy.Date_Ascending:

                    allData = allData.OrderBy(x => x.Entry.CreationTime);

                    break;
                case CommentSortStrategy.Date_Descending:

                    allData = allData.OrderByDescending(x => x.Entry.CreationTime);

                    break;
                case CommentSortStrategy.Likes_Descending:

                    allData = allData.OrderByDescending(x => x.Entry.Likes.Count);

                    break;
                case CommentSortStrategy.Likes_Ascending:

                    allData = allData.OrderBy(x => x.Entry.Likes.Count);

                    break;
            }

            if (fromEntry > 0 && entries > 0)
            {
                allData = allData.Skip(fromEntry).Take(entries);
            }

            return GetDTOListFrom(allData);

        }

        protected override DbSet<Comment> GetDataSet()
        {
            return this.dbContext.Comments;
        }
    }
}
