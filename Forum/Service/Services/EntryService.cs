﻿using Data.Contracts;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Service.Contracts;
using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class EntryService : BaseService<Entry, EntryDTO>, IEntryService
    {
        public EntryService(IDataContext database) : base(database) { }
        protected override IQueryable<Entry> GetAllData()
        {

            return this.dbContext.Entries
                .Include(x => x.Author).ThenInclude(x => x.UserPrivilege)
                .Include(x => x.Likes).ThenInclude(x => x.Author).ThenInclude(x => x.UserPrivilege);
        }

        protected override DbSet<Entry> GetDataSet()
        {
            return this.dbContext.Entries;
        }
        
        public override void AddToDbContext(Entry data)
        {
            this.dbContext.Entries.Add(data);
        }
    }
}
