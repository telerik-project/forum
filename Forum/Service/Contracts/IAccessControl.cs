﻿using Data.Models;
using Service.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Contracts
{
    public interface IAccessControl
    {
        public UserDTO TryGetByUsername(string username);

        public Service.Enums.UserPrivilegeLevel GetUserPrivilegeLevel(string username);

        public bool IsAdmin(UserDTO user);
    }
}
