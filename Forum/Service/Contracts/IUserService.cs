﻿using Data.Models;
using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Contracts
{
    public interface IUserService : IBaseService<UserDTO>
    {
        public UserDTO GetByUsername(string username);
        public bool CheckForAvailability(string username, string email, string displayName);
        public User SetProfilePicture(string username, string relativePath);

        public IEnumerable<UserDTO> GetAll(UserFilterStrategy filterStrategy = UserFilterStrategy.None, string filterInput = "", int fromEntry = -1, int entries = -1);

        public UserDTO AuthenticateUser(string username, string password);

        public void PopulateUserDTO(UserDTO dto);
        public void BlockUser(int id);
        public void UnblockUser(int id);




    }
}
