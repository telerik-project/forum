﻿using Service.DtoModels;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Contracts
{
    public interface IPostService : IAuthoredService<PostDTO>
    {
        public IEnumerable<PostDTO> GetAll(PostSortStrategy sortStrategy = PostSortStrategy.None, PostFilterStrategy filterStrategy = PostFilterStrategy.None, string filterData = "", int fromEntry = -1, int entries = -1);
    }
}
