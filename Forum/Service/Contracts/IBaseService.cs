﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Contracts
{
    public interface IBaseService<TDTO>
    {
        TDTO Get(int id);

        IEnumerable<TDTO> GetAll();

        int GetRecordCount();

        TDTO Create(TDTO data);

        TDTO Update(int id, TDTO data);

        void Delete(int id);
    }
}
