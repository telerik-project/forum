﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DtoModels
{
    public class LikeDTO : BaseDTO<Like>
    {
        public int EntryId { get; set; }
        public int AuthorId { get; set; }
        public UserDTO Author { get; set; }
        public DateTime CreationTime { get; set; }
        public LikeDTO(Like data) : base(data) { }
        public LikeDTO() {

            CreationTime = DateTime.Now;

        }


        public override Like ToData()
        {
            return new Like
            {
                //Author = Author.ToData(),
                EntryId = this.EntryId,
                AuthorId = this.AuthorId,
                CreationTime = this.CreationTime,
                //Entry = Entry.ToData()
            };
        }

        protected override void FromData(Like data)
        {
            this.Id = data.Id;
            this.Author = new UserDTO(data.Author);
            this.AuthorId = data.AuthorId;
            this.CreationTime = data.CreationTime;
            //this.Entry = new EntryDTO(data.Entry);
            this.EntryId = data.EntryId;
        }

        public override bool WasCreatedBy(UserDTO user)
        {
            return this.Author.Id == user.Id;
        }
    }
}
