﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DtoModels
{
    public class UserDTO : BaseDTO<User>
    {
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ProfilePicture { get; set; }

        public int LikeCount { get; set; }
        public int CommentCount { get; set; }
        public int PostCount { get; set; }

        public UserPrivilegeDTO UserPrivilege { get; set; }
        public UserDTO(User data) : base(data) { }
        public UserDTO() {

            UserPrivilege = new UserPrivilegeDTO();
        
        }
        public override User ToData()
        {
            return new User
            {
                Username = this.Username,
                DisplayName = this.DisplayName,
                Email = this.Email,
                Password = this.Password,
                UserPrivilege = this.UserPrivilege.ToData(),
                ProfilePicture = this.ProfilePicture
            };
        }

        protected override void FromData(User data)
        {
            
            this.Id = data.Id;
            this.Username = data.Username;
            this.DisplayName = data.DisplayName;
            this.Email = data.Email;
            this.Id = data.Id;
            this.Password = data.Password;
            this.UserPrivilege = new UserPrivilegeDTO(data.UserPrivilege);
            this.ProfilePicture = data.ProfilePicture;
        }

        public override bool WasCreatedBy(UserDTO user)
        {
            return this.Id == user.Id;
        }
    }
}
