﻿using Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DtoModels
{
    public abstract class BaseDTO<T> : IUserCreatedModel
    {
        public int Id { get; set; }

        public abstract T ToData();

        protected abstract void FromData(T data);

        public BaseDTO() { }
        public BaseDTO(T data) {
            FromData(data);
        }

        public abstract bool WasCreatedBy(UserDTO user);

    }
}
