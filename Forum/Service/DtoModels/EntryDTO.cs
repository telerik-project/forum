﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DtoModels
{
    public class EntryDTO : BaseDTO<Entry>
    {
        public int AuthorId { get; set; }
        public UserDTO Author { get; set; }
        public string Content { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime EditTime { get; set; }

        //And a list of likes
        public List<LikeDTO> Likes { get; set; }

        public EntryDTO(Entry data) : base(data) { }
        public EntryDTO() {

            CreationTime = DateTime.Now;
            EditTime = DateTime.Now;
        
        }

        public override Entry ToData()
        {

            List<Like> likes = new List<Like>();

            if (Likes != null)
            {

                for (int i = 0; i < Likes.Count; i++)
                {
                    likes.Add(Likes[i].ToData());
                }

            }

            return new Entry
            {
                AuthorId = this.AuthorId,
                Content = this.Content,
                CreationTime = this.CreationTime,
                EditTime = this.EditTime,
                Likes = likes
            };
        }

        protected override void FromData(Entry data)
        {
            this.Id = data.Id;
            this.Author = new UserDTO(data.Author);
            this.AuthorId = data.AuthorId;
            this.Content = data.Content;
            this.CreationTime = data.CreationTime;
            this.EditTime = data.EditTime;

            this.Likes = new List<LikeDTO>();

            if(data.Likes == null)
            {
                return;
            }

            for(int i = 0; i < data.Likes.Count; i++)
            {
                if (data.Likes[i].IsDeleted)
                {
                    continue;
                }

                this.Likes.Add(new LikeDTO(data.Likes[i]));
            }

        }

        public override bool WasCreatedBy(UserDTO user)
        {
            return this.Author.Id == user.Id;
        }
    }
}
