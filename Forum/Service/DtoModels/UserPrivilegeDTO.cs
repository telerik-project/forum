﻿using Data.Models;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DtoModels
{
    public class UserPrivilegeDTO : BaseDTO<UserPrivilege>
    {
        public int UserId { get; set; }

        public UserPrivilegeLevel Privilege { get; set; }

        public UserPrivilegeDTO(UserPrivilege data) : base(data) { }
        public UserPrivilegeDTO() { }
        public override UserPrivilege ToData()
        {
            return new UserPrivilege
            {
                UserId = this.UserId,
                //User = User.ToData(),
                Privilege = (int)this.Privilege
            };
        }

        protected override void FromData(UserPrivilege data)
        {

            if(data == null){
                return;
            }

            this.Id = data.Id;
            this.UserId = data.UserId;
            this.Privilege = (UserPrivilegeLevel)data.Privilege;
        }

        public override bool WasCreatedBy(UserDTO user)
        {
            return this.UserId == user.Id;
        }
    }
}
