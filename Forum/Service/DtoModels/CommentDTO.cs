﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DtoModels
{
    public class CommentDTO : BaseDTO<Comment>
    {
        public int PostId { get; set; }

        public int EntryId { get; set; }

        public EntryDTO Entry { get; set; }

        public CommentDTO(Comment c) : base(c) { }
        public CommentDTO() { }

        public override Comment ToData()
        {
            return new Comment
            {
                PostId = this.PostId,
                EntryId = this.EntryId,
                Entry = this.Entry.ToData()
            };
        }

        protected override void FromData(Comment data)
        {
            this.Id = data.Id;
            this.PostId = data.PostId;
            //this.Post = new PostDTO(data.Post);
            this.EntryId = data.EntryId;
            this.Entry = new EntryDTO(data.Entry);
        }

        public override bool WasCreatedBy(UserDTO user)
        {
            return this.Entry.Author.Id == user.Id;
        }
    }
}
