﻿using Data.Models;
using Service.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DtoModels
{
    public class PostDTO : BaseDTO<Post>
    {
        public List<CommentDTO> Comments { get; set; }
        public string Title { get; set; }
        public int EntryId { get; set; }
        public EntryDTO Entry { get; set; }
        public PostDTO(Post data) : base(data) { }
        public PostDTO() { }

        public override Post ToData()
        {

            return new Post
            {
                Title = this.Title,
                EntryId = this.EntryId,
                Entry = Entry.ToData()
            };
        }

        protected override void FromData(Post data)
        {

            this.Id = data.Id;
            this.Title = data.Title;
            this.EntryId = data.EntryId;
            this.Entry = new EntryDTO(data.Entry);

            List<CommentDTO> comments = new List<CommentDTO>();

            if (data.Comments != null)
            {

                for (int i = 0; i < data.Comments.Count; i++)
                {

                    if (data.Comments[i].IsDeleted)
                    {
                        continue;
                    }

                    comments.Add(new CommentDTO(data.Comments[i]));
                }

            }

            this.Comments = comments;


        }

        public void ApplySortingToComments(CommentSortStrategy sortStrategy)
        {
            switch (sortStrategy)
            {
                case CommentSortStrategy.Date_Ascending:

                    Comments = Comments.OrderBy(x => x.Entry.CreationTime).ToList();

                    break;
                case CommentSortStrategy.None:
                case CommentSortStrategy.Date_Descending:

                    Comments = Comments.OrderByDescending(x => x.Entry.CreationTime).ToList();

                    break;
                case CommentSortStrategy.Likes_Descending:

                    Comments = Comments.OrderByDescending(x => x.Entry.Likes.Count).ToList();

                    break;
                case CommentSortStrategy.Likes_Ascending:

                    Comments = Comments.OrderBy(x => x.Entry.Likes.Count).ToList();

                    break;
            }
        }

        public override bool WasCreatedBy(UserDTO user)
        {
            return this.Entry.Author.Id == user.Id;
        }
    }
}
