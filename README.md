# Forum

Forum is an online discussion site where people can hold conversations by posting, commenting, and liking messages.

## Getting started

Required software: Visual Studio, MS SQL Managment Studio 18

The first step is to clone the repository in a local folder of your choice. <br />
Next is setting up the connection string. <br />
In order to find out what your SQL Server name is, you need to open **SQL Server Configuration Manager**.<br />
Navigate to SQL Server Services.<br />
By default you should have SQL Server(MSSQLSERVER)<br />
Copy the name inside the brackets and then navigate to **appsettings.json** inside the Web project using Visual Studio.<br />
On line 11 where the default connection is replace the value SQLEXPRESS with the value you copied.<br />
`"DefaultConnection": "Server=.\\SQLEXPRESS;Database=UltraForum;Trusted_Connection=True;",`<br />


## Database
There are 2 options of creating the database and seeding data.

**Option number 1 using .sql scripts:**

First we need to execute a query in SQL Server Management Studio 18 using the file **Create_Database.sql** <br />
This will create new database with its tables and relations. <br />
Second we need another query for seeding the data using the file **Seed_Data.sql** <br />
Keep in mind that the seed data is minimal and it is only for demonstration purpose.

**Option number 2 using code first aproach with modelbuilder:**

First we need to set **Web** as starting project <br />
The next step is to open Package Manager Console and set **Data** as Default Project <br />
Now we want to execute the command **add-migration Initial;update-database** <br />


You are now ready to start up the project and try it out.
